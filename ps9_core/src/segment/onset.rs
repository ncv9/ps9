//! Sequences of consonants that can start a syllable in Ŋarâþ Crîþ.

use std::{
    fmt::{Display, Write},
    hint::unreachable_unchecked,
    mem,
};

use enum_iterator::Sequence;
use static_assert_macro::static_assert;

use super::cons::{Consonant, Epf, Liquid};

/// An optional ⟦j⟧ consonant.
#[repr(u8)]
#[derive_const(Clone, PartialEq)]
#[derive(Copy, Debug, Eq, Hash, Sequence)]
pub enum Medial {
    /// The ⟦j⟧ is not present.
    Empty,
    /// The ⟦j⟧ is present.
    J,
}

/// A sequence of consonants that can start a syllable, not including the medial.
///
/// This structure is designed to occupy a single byte, as there are fewer than 256 possible initials. It can be converted to an enum that can be matched on using the [`Initial::expand`] method. Conversely, [`ExpandedInitial`] can be converted back to [`Initial`] using [`ExpandedInitial::compact`].
///
/// # Representation
///
/// The underlying value can hold one of the following bit patterns:
///
/// * 00xxxxxx for [`ExpandedInitial::C`] (single consonant)
/// * 010xxxxx for [`ExpandedInitial::Cl`] ([`Epf`] + ⟦r⟧)
/// * 011xxxxx for [`ExpandedInitial::Cl`] ([`Epf`] + ⟦l⟧)
/// * 1xxxxxxx for empty and other onsets:
///   * 128 for [`ExpandedInitial::Empty`]
///   * 129 for [`ExpandedInitial::Cf`]
///   * 130 for [`ExpandedInitial::Cþ`]
///   * 131 for [`ExpandedInitial::Cs`]
///   * 132 for [`ExpandedInitial::Cš`]
///   * 133 for [`ExpandedInitial::Gv`]
///   * 134 for [`ExpandedInitial::Gð`]
///   * 135 for [`ExpandedInitial::Tf`]
///   * 136 for [`ExpandedInitial::Dv`]
///
/// The payload in each case must be in bounds.
#[repr(transparent)]
#[derive_const(Clone, PartialEq)]
#[derive(Copy, Debug, Eq, Hash, Sequence)]
pub struct Initial(u8);

/// The expanded form of an [`Initial`], suitable for matching.
///
/// See [`Initial`] for more details.
#[derive_const(Clone, PartialEq)]
#[derive(Copy, Debug, Eq, Hash, Sequence)]
#[allow(missing_docs)]
pub enum ExpandedInitial {
    /// The empty initial.
    Empty,
    /// A single-consonant initial.
    C(Consonant),
    /// An initial consisting of an effective plosive or fricative followed by ⟦r⟧ or ⟦l⟧.
    Cl(Epf, Liquid),
    Cf,
    Cþ,
    Cs,
    Cš,
    Gv,
    Gð,
    Tf,
    Dv,
}

const MAX1: u8 = Consonant::CARDINALITY as u8 - 1;
const MAX2: u8 = 64 + Epf::CARDINALITY as u8 - 1;
const MAX3: u8 = 96 + Epf::CARDINALITY as u8 - 1;

impl Initial {
    /// Converts a byte to an [`Initial`] without checking its validity.
    ///
    /// # Safety
    ///
    /// `val` must specify a valid bit pattern for [`Initial`].
    #[inline]
    pub const unsafe fn new_unchecked(val: u8) -> Self {
        Self(val)
    }

    /// Returns the byte representation of this [`Initial`].
    #[inline]
    pub const fn repr(self) -> u8 {
        self.0
    }

    /// Converts an [`Initial`] to an [`ExpandedInitial`].
    #[inline]
    pub const fn expand(self) -> ExpandedInitial {
        let x = self.0;
        match x {
            0..=MAX1 => unsafe { ExpandedInitial::C(mem::transmute(x)) },
            64..=MAX2 => unsafe { ExpandedInitial::Cl(mem::transmute(x - 64), Liquid::R) },
            96..=MAX3 => unsafe { ExpandedInitial::Cl(mem::transmute(x - 96), Liquid::L) },
            128 => ExpandedInitial::Empty,
            129 => ExpandedInitial::Cf,
            130 => ExpandedInitial::Cþ,
            131 => ExpandedInitial::Cs,
            132 => ExpandedInitial::Cš,
            133 => ExpandedInitial::Gv,
            134 => ExpandedInitial::Gð,
            135 => ExpandedInitial::Tf,
            136 => ExpandedInitial::Dv,
            _ => unsafe { unreachable_unchecked() },
        }
    }
}

impl ExpandedInitial {
    /// Converts an [`ExpandedInitial`] to an [`Initial`].
    #[inline]
    pub const fn compact(self) -> Initial {
        Initial(match self {
            ExpandedInitial::Empty => 128,
            ExpandedInitial::C(c) => c as u8,
            ExpandedInitial::Cl(c, r) => 64 | ((r as u8) << 5) | (c as u8),
            ExpandedInitial::Cf => 129,
            ExpandedInitial::Cþ => 130,
            ExpandedInitial::Cs => 131,
            ExpandedInitial::Cš => 132,
            ExpandedInitial::Gv => 133,
            ExpandedInitial::Gð => 134,
            ExpandedInitial::Tf => 135,
            ExpandedInitial::Dv => 136,
        })
    }
}

impl const From<Initial> for ExpandedInitial {
    #[inline]
    fn from(value: Initial) -> Self {
        value.expand()
    }
}

impl const From<ExpandedInitial> for Initial {
    #[inline]
    fn from(value: ExpandedInitial) -> Self {
        value.compact()
    }
}

// TODO: perhaps a lookup table would be better for displaying initials
impl Display for Initial {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.expand(), f)
    }
}

impl Display for ExpandedInitial {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ExpandedInitial::Empty => Ok(()),
            ExpandedInitial::C(c) => Display::fmt(c, f),
            ExpandedInitial::Cl(c, l) => {
                Display::fmt(c, f)?;
                Display::fmt(l, f)
            }
            ExpandedInitial::Cf => f.write_str("cf"),
            ExpandedInitial::Cþ => f.write_str("cþ"),
            ExpandedInitial::Cs => f.write_str("cs"),
            ExpandedInitial::Cš => f.write_str("cš"),
            ExpandedInitial::Gv => f.write_str("gv"),
            ExpandedInitial::Gð => f.write_str("gð"),
            ExpandedInitial::Tf => f.write_str("tf"),
            ExpandedInitial::Dv => f.write_str("dv"),
        }
    }
}

impl Display for Medial {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Medial::Empty => Ok(()),
            Medial::J => f.write_char('j'),
        }
    }
}

static_assert!(mem::size_of::<Medial>() == 1);
static_assert!(mem::size_of::<Initial>() == 1);
