//! Contains types to describe parts of syllables in Ŋarâþ Crîþ v9.

pub mod coda;
pub mod cons;
pub mod decoration;
pub mod mgp;
pub mod onset;
mod private;
pub mod vowel;
