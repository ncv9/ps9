//! Vowels in Ŋarâþ Crîþ. See [`Vowel`] for more details.

use std::{
    fmt::{Display, Write},
    mem,
};

use static_assert_macro::static_assert;

#[doc(inline)]
pub use super::private::Vowel;

static_assert!(mem::size_of::<Vowel>() == 1);

macro_rules! define_vowel_transform {
    ($doc:expr, $name:ident, $($from:ident => $to:ident),*) => {
        #[doc = $doc]
        #[inline]
        pub const fn $name(self) -> Self {
            match self {
                $(Vowel::$from => Vowel::$to,)*
                _ => self,
            }
        }
    }
}

impl Vowel {
    // as_char defined in private module

    define_vowel_transform!("Applies the *π*-transformation to this vowel.", pi, A => O, O => E, E => I, Â => Ô, Ô => Ê, Ê => Î);
    define_vowel_transform!("Applies the *γ*-transformation to this vowel.", gam, A => E, O => E, E => I, I => E, Â => Ê, Ô => Ê, Ê => Î, Î => Ê);
    define_vowel_transform!("Applies the *λ*-transformation to this vowel.", lam, O => A, I => E, Ô => Â, Î => Ê);
    define_vowel_transform!("Applies the plus-transformation to this vowel.", plus, O => A, Ô => Â);
    define_vowel_transform!("Applies the *κ*-transformation to this vowel.", kappa, O => E, Ô => Ê);
    define_vowel_transform!("Applies the *τ*-transformation to this vowel.", tau, O => E, I => E, Ô => Ê, Î => Ê);
    define_vowel_transform!("Applies the *φ*-transformation to this vowel.", phi, A => E, I => A, Â => Ê, Î => Â);
    define_vowel_transform!("Applies the *ψ*-transformation to this vowel.", psi, A => I, I => O, Â => Î, Î => Ô);
    define_vowel_transform!("Applies the *η*-transformation to this vowel.", eta, A => Â, E => Ê, I => Î, O => Ô);
}

impl Display for Vowel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_char(self.as_char())
    }
}
