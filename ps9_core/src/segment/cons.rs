//! Various consonant classes in Ŋarâþ Crîþ.

use std::fmt::Display;

#[doc(inline)]
pub use super::private::Consonant;

impl Display for Consonant {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}

#[doc(inline)]
pub use super::private::Epf;

impl Display for Epf {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}

#[doc(inline)]
pub use super::private::Liquid;

impl Display for Liquid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}
