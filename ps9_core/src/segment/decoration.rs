//! A set of markers that can modify a word.

use std::fmt::{Display, Write};

/// A set of markers that can modify a word.
///
/// See [the module documentation][`self`] for more.
#[derive_const(Clone, PartialEq, Default)]
#[derive(Copy, Eq, Hash, Debug)]
#[cfg_attr(test, derive(proptest_derive::Arbitrary))]
pub struct Decoration {
    /// Any name markers occuring at the start of the word.
    pub premarker: PreMarker,
    /// True if a *nef*, ⟦*⟧, is present.
    pub has_nef: bool,
    /// True if a *sen*, ⟦&⟧, is present.
    pub has_sen: bool,
}

impl Decoration {
    /// Returns true if this is the empty decoration.
    pub const fn is_empty(&self) -> bool {
        *self == Decoration::default()
    }
}

impl Display for Decoration {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.has_nef {
            f.write_char('*')?;
        }
        Display::fmt(&self.premarker, f)?;
        if self.has_sen {
            f.write_char('&')?;
        }
        Ok(())
    }
}

/// A name marker that can occur at the start of a word, or the absence thereof.
///
/// This excludes ⟦*⟧ and ⟦&⟧, which are orthogonal to all of the markers in this enum.
#[derive_const(Clone, PartialEq, Default)]
#[derive(Copy, Eq, Hash, Debug)]
#[cfg_attr(test, derive(proptest_derive::Arbitrary))]
pub enum PreMarker {
    /// No marker at all.
    #[default]
    None,
    /// ⟦#⟧
    Carþ,
    /// ⟦+⟧
    Tor,
    /// ⟦+*⟧
    Njor,
    /// ⟦@⟧
    Es,
}

impl PreMarker {
    /// Returns the string representation of this marker.
    pub const fn as_str(self) -> &'static str {
        match self {
            PreMarker::None => "",
            PreMarker::Carþ => "#",
            PreMarker::Tor => "+",
            PreMarker::Njor => "+*",
            PreMarker::Es => "@",
        }
    }
}

impl Display for PreMarker {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}
