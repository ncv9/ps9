//! Manifested grapheme phrases in Ŋarâþ Crîþ.

use std::fmt::Display;

#[doc(inline)]
pub use super::private::Mgp;

impl Display for Mgp {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}
