//! Contains functionality for dealing with assemblages.
//!
//! # Overview
//!
//! An *assemblage* is a part of a word. It can be described as a path through a state machine with five states: *s* (syllabic), *g* (glide), *o* (onset), *n* (nuclear), and *ω* (terminal). Each transition in the state machine is associated with its own kind of payload.
//!
//! Both [`Asb`] and [`Assemblage`] take three type parameters: the start and end states, plus the minimum guaranteed length of the assemblage. The actual length of an assemblage may be greater than the minimum but may not be less than it.
//!
//! Assemblages are internally backed by byte arrays: either [`[u8]`][slice] for [`Asb`] or [`SmallVec<[u8; 16]>`][SmallVec] for [`Assemblage`].
//!
//! ## Further reading
//!
//! [Layer 0: the assemblage structure](http://ncv9.flirora.xyz/grammar/phonology/layer0.html) from the Ŋarâþ Crîþ v9 grammar

#![allow(clippy::type_complexity)]

use std::{
    borrow::{Borrow, BorrowMut},
    fmt::{self, Debug, Display},
    hint::unreachable_unchecked,
    marker::{Destruct, PhantomData},
    mem,
    ops::{ControlFlow, Deref, DerefMut},
};

use core_extensions::utils;
use smallvec::SmallVec;
use static_assert_macro::static_assert;
use type_equalities::{type_functions::TypeFunction, TypeEq};

use crate::{
    control::{ControlFlowExt, ResultExt},
    conv::TooShort,
    segment::{
        coda::{Coda, TerminalCoda},
        onset::{Initial, Medial},
        vowel::Vowel,
    },
};

use self::_private::{Sealed, SealedPayload};

mod _private {
    // TODO: replace with official construct for sealed traits when it comes
    pub trait Sealed {}
    pub trait SealedPayload {}
}

/// The maximum number of elements that can be stored in an [`Assemblage`] inline before a heap allocation is required.
///
/// This is the maximum value that would not increase the size of [`Assemblage`] on the stack on x86_64 platforms.
const ASMSVSZ: usize = 16;

/// Updates the minimum guaranteed length given a nonempty assemblage.
///
/// This is used in methods such as [`Asb::try_split_first`].
#[inline]
pub const fn s1a3(s: usize) -> usize {
    if s == 0 {
        s + 3
    } else {
        s - 1
    }
}

/// A runtime marker for a state in the state machine.
///
/// Each state can also be associated with additional data.
#[allow(missing_docs)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum StateE<S = (), G = (), O = (), N = (), Om = ()> {
    Syl(S),
    Gld(G),
    Ons(O),
    Nuc(N),
    Trm(Om),
}

impl<T> StateE<T, T, T, T, T> {
    /// If all variants have the same payload type, then returns the payload as that type.
    #[inline]
    pub const fn unify(self) -> T
    where
        T: ~const Destruct,
    {
        match self {
            StateE::Syl(t) => t,
            StateE::Gld(t) => t,
            StateE::Ons(t) => t,
            StateE::Nuc(t) => t,
            StateE::Trm(t) => t,
        }
    }
}

/// A trait implemented by states of the state machine.
///
/// This trait is sealed and may not be implemented by downstream types.
#[const_trait]
pub trait State: Sealed + Copy + Eq + ~const Clone + ~const PartialEq + ~const Default {
    /// The unique predecessor of this state.
    type Predecessor: ~const State;
    /// The [payload][Payload] associated with the transition from the predecessor state ot this one.
    type PPayload: Payload<Successor = Self>;
    /// Selects one of the given types depending on the state.
    type Select<S, G, O, N, Om>;
    /// Selects one of the given values depending on the state.
    ///
    /// Each of the callbacks takes a [witness type for equality][`TypeEq`] between `Self` and the particular state type. In addition, `val` is passed to the appropriate closure.
    fn select<S, G, O, N, Om, X>(
        s: impl ~const FnOnce(TypeEq<Self, Syl>, X) -> S + ~const Destruct,
        g: impl ~const FnOnce(TypeEq<Self, Gld>, X) -> G + ~const Destruct,
        o: impl ~const FnOnce(TypeEq<Self, Ons>, X) -> O + ~const Destruct,
        n: impl ~const FnOnce(TypeEq<Self, Nuc>, X) -> N + ~const Destruct,
        om: impl ~const FnOnce(TypeEq<Self, Trm>, X) -> Om + ~const Destruct,
        val: X,
    ) -> StateE<S, G, O, N, Om>;
}

/// A trait for states that have a unique successor given that the assemblage that it is in ends in the state `E`.
// TODO: it might be worth adding a few generic types so we can express things like ‘has a unique successor for assemblages that don’t end in the terminal state’ or ‘has a unique successor for assemblages that have at least 2 more elements’
#[const_trait]
pub trait Sss<E: State, const MIN_LEN: usize>: ~const State {
    /// The unique successor of this state.
    type Successor: ~const State<PPayload = Self::SPayload>;
    /// The [payload][Payload] associated with the transition from this state to the successor.
    type SPayload: Payload<Predecessor = Self>;
}

/// A trait for states other than the terminal state.
#[const_trait]
pub trait Nts: ~const State {}

/// A payload for a transition between two [states][State].
///
/// In the state machine, a payload is data associated with the transition from one state to another. The payload consists of a segment; for instance, the transition from the onset to the nuclear state has a payload of a [vowel][Vowel].
///
/// All payloads in ps9’s state machine are exactly 1 byte long and implement [`Copy`]. Each transition in the state machine has a unique payload.
pub trait Payload: Copy + SealedPayload {
    /// The state that this payload transitions from.
    type Predecessor: ~const State;
    /// The state that this payload transitions to.
    type Successor: ~const State<PPayload = Self, Predecessor = Self::Predecessor>;
}

/// The syllabic (*s*) state of the state machine.
#[derive_const(Clone, PartialEq, Default)]
#[derive(Copy, Eq)]
pub struct Syl;
impl Sealed for Syl {}
impl const State for Syl {
    type Predecessor = Nuc;
    type PPayload = Coda;
    type Select<S, G, O, N, Om> = S;
    #[inline]
    fn select<S, G, O, N, Om, X>(
        s: impl ~const FnOnce(TypeEq<Self, Syl>, X) -> S + ~const Destruct,
        _g: impl ~const FnOnce(TypeEq<Self, Gld>, X) -> G + ~const Destruct,
        _o: impl ~const FnOnce(TypeEq<Self, Ons>, X) -> O + ~const Destruct,
        _n: impl ~const FnOnce(TypeEq<Self, Nuc>, X) -> N + ~const Destruct,
        _om: impl ~const FnOnce(TypeEq<Self, Trm>, X) -> Om + ~const Destruct,
        val: X,
    ) -> StateE<S, G, O, N, Om> {
        StateE::Syl(s(TypeEq::refl(), val))
    }
}
impl<E: State, const MIN_LEN: usize> const Sss<E, MIN_LEN> for Syl {
    type Successor = Gld;
    type SPayload = Initial;
}
impl const Nts for Syl {}
/// The glide (*g*) state of the state machine.
#[derive_const(Clone, PartialEq, Default)]
#[derive(Copy, Eq)]
pub struct Gld;
impl Sealed for Gld {}
impl const State for Gld {
    type Predecessor = Syl;
    type PPayload = Initial;
    type Select<S, G, O, N, Om> = G;
    #[inline]
    fn select<S, G, O, N, Om, X>(
        _s: impl ~const FnOnce(TypeEq<Self, Syl>, X) -> S + ~const Destruct,
        g: impl ~const FnOnce(TypeEq<Self, Gld>, X) -> G + ~const Destruct,
        _o: impl ~const FnOnce(TypeEq<Self, Ons>, X) -> O + ~const Destruct,
        _n: impl ~const FnOnce(TypeEq<Self, Nuc>, X) -> N + ~const Destruct,
        _om: impl ~const FnOnce(TypeEq<Self, Trm>, X) -> Om + ~const Destruct,
        val: X,
    ) -> StateE<S, G, O, N, Om> {
        StateE::Gld(g(TypeEq::refl(), val))
    }
}
impl<E: State, const MIN_LEN: usize> const Sss<E, MIN_LEN> for Gld {
    type Successor = Ons;
    type SPayload = Medial;
}
impl const Nts for Gld {}
/// The onset (*o*) state of the state machine.
#[derive_const(Clone, PartialEq, Default)]
#[derive(Copy, Eq)]
pub struct Ons;
impl Sealed for Ons {}
impl const State for Ons {
    type Predecessor = Gld;
    type PPayload = Medial;
    type Select<S, G, O, N, Om> = O;
    #[inline]
    fn select<S, G, O, N, Om, X>(
        _s: impl ~const FnOnce(TypeEq<Self, Syl>, X) -> S + ~const Destruct,
        _g: impl ~const FnOnce(TypeEq<Self, Gld>, X) -> G + ~const Destruct,
        o: impl ~const FnOnce(TypeEq<Self, Ons>, X) -> O + ~const Destruct,
        _n: impl ~const FnOnce(TypeEq<Self, Nuc>, X) -> N + ~const Destruct,
        _om: impl ~const FnOnce(TypeEq<Self, Trm>, X) -> Om + ~const Destruct,
        val: X,
    ) -> StateE<S, G, O, N, Om> {
        StateE::Ons(o(TypeEq::refl(), val))
    }
}
impl<E: State, const MIN_LEN: usize> const Sss<E, MIN_LEN> for Ons {
    type Successor = Nuc;
    type SPayload = Vowel;
}
impl const Nts for Ons {}
/// The nuclear (*n*) state of the state machine.
///
/// This state has two possible successors: the syllabic and the terminal.
///
/// However, which state it leads to is not stored in the assemblage because it is guaranteed to be the terminal state if there is one element after the state and the syllabic state if there is more than one element.
///
/// In addition, the nuclear state cannot lead to a terminal state if the assemblage ends in a state other than the terminal state.
#[derive_const(Clone, PartialEq, Default)]
#[derive(Copy, Eq)]
pub struct Nuc;
impl Sealed for Nuc {}
impl const State for Nuc {
    type Predecessor = Ons;
    type PPayload = Vowel;
    type Select<S, G, O, N, Om> = N;
    #[inline]
    fn select<S, G, O, N, Om, X>(
        _s: impl ~const FnOnce(TypeEq<Self, Syl>, X) -> S + ~const Destruct,
        _g: impl ~const FnOnce(TypeEq<Self, Gld>, X) -> G + ~const Destruct,
        _o: impl ~const FnOnce(TypeEq<Self, Ons>, X) -> O + ~const Destruct,
        n: impl ~const FnOnce(TypeEq<Self, Nuc>, X) -> N + ~const Destruct,
        _om: impl ~const FnOnce(TypeEq<Self, Trm>, X) -> Om + ~const Destruct,
        val: X,
    ) -> StateE<S, G, O, N, Om> {
        StateE::Nuc(n(TypeEq::refl(), val))
    }
}
/// In an assemblage that does not end in the terminal state, the nuclear state may only lead to the syllabic state.
impl<E: Nts, const MIN_LEN: usize> const Sss<E, MIN_LEN> for Nuc {
    type Successor = Syl;
    type SPayload = Coda;
}
/// If a nuclear state has at least two elements following it, then its successor is always the syllabic state.
impl<const MIN_LEN: usize> const Sss<Trm, MIN_LEN> for Nuc
where
    [(); MIN_LEN - 2]:,
{
    type Successor = Syl;
    type SPayload = Coda;
}
impl const Nts for Nuc {}
/// The terminal (*ω*) state of the state machine.
#[derive_const(Clone, PartialEq, Default)]
#[derive(Copy, Eq)]
pub struct Trm;
impl Sealed for Trm {}
impl const State for Trm {
    type Predecessor = Nuc;
    type PPayload = TerminalCoda;
    type Select<S, G, O, N, Om> = Om;
    #[inline]
    fn select<S, G, O, N, Om, X>(
        _s: impl ~const FnOnce(TypeEq<Self, Syl>, X) -> S + ~const Destruct,
        _g: impl ~const FnOnce(TypeEq<Self, Gld>, X) -> G + ~const Destruct,
        _o: impl ~const FnOnce(TypeEq<Self, Ons>, X) -> O + ~const Destruct,
        _n: impl ~const FnOnce(TypeEq<Self, Nuc>, X) -> N + ~const Destruct,
        om: impl ~const FnOnce(TypeEq<Self, Trm>, X) -> Om + ~const Destruct,
        val: X,
    ) -> StateE<S, G, O, N, Om> {
        StateE::Trm(om(TypeEq::refl(), val))
    }
}
/// The terminal state has no successors.
impl<E: State, const MIN_LEN: usize> !Sss<E, MIN_LEN> for Trm {}
impl !Nts for Trm {}

impl SealedPayload for Initial {}
impl Payload for Initial {
    type Predecessor = Syl;
    type Successor = Gld;
}
impl SealedPayload for Medial {}
impl Payload for Medial {
    type Predecessor = Gld;
    type Successor = Ons;
}
impl SealedPayload for Vowel {}
impl Payload for Vowel {
    type Predecessor = Ons;
    type Successor = Nuc;
}
impl SealedPayload for Coda {}
impl Payload for Coda {
    type Predecessor = Nuc;
    type Successor = Syl;
}
impl SealedPayload for TerminalCoda {}
impl Payload for TerminalCoda {
    type Predecessor = Nuc;
    type Successor = Trm;
}

/// A slice of an assemblage (akin to [`str`]).
// TODO: this can become const `PartialEq` once slices start implementing that trait constly
#[repr(transparent)]
#[derive(PartialEq, Eq)]
pub struct Asb<S: ~const State, E: ~const State, const MIN_LEN: usize> {
    _phantom: PhantomData<(S, E)>,
    payloads: [u8],
}

/// An owned assemblage (akin to [`String`]).
#[derive(Clone, PartialEq, Eq)]
pub struct Assemblage<S: ~const State, E: ~const State, const MIN_LEN: usize> {
    _phantom: PhantomData<(S, E)>,
    payloads: SmallVec<[u8; ASMSVSZ]>,
}

#[cfg(target_arch = "x86_64")]
static_assert!(mem::size_of::<Assemblage<Syl, Trm, 0>>() == 24);

impl<S: ~const State, E: ~const State, const MIN_LEN: usize> Asb<S, E, MIN_LEN> {
    /// Returns the length of this assemblage.
    ///
    /// This is guaranteed to be at least `MIN_LEN`.
    #[inline]
    pub const fn len(&self) -> usize {
        self.payloads.len()
    }

    /// Returns true if this assemblage is empty.
    ///
    /// This is always false when `MIN_LEN > 0`.
    #[inline]
    pub const fn is_empty(&self) -> bool {
        MIN_LEN == 0 && self.payloads.is_empty()
    }

    /// Converts a reference to a byte slice to an assemblage reference.
    ///
    /// # Safety
    ///
    /// * `slice` must be at least `MIN_LEN` elements long.
    /// * `slice` must contain valid data given the start and end states.
    #[inline]
    pub const unsafe fn new_unchecked(slice: &[u8]) -> &Self {
        mem::transmute(slice)
    }

    /// Converts a mutable reference to a byte slice to an assemblage reference.
    ///
    /// # Safety
    ///
    /// * `slice` must be at least `MIN_LEN` elements long.
    /// * `slice` must contain valid data given the start and end states.
    #[inline]
    pub const unsafe fn new_unchecked_mut(slice: &mut [u8]) -> &mut Self {
        mem::transmute(slice)
    }

    /// Returns the raw byte representation of this assemblage.
    #[inline]
    pub const fn as_raw(&self) -> &[u8] {
        &self.payloads
    }

    /*
        Ujinuc ne Urerasufy Iucotih Ynonan.
        Etih Sowod Ihsataw wa Yanebat na?
        Eatoc ma Zaracaw Atto Siroy.
    */

    const unsafe fn try_split_first_raw<S2: ~const State>(
        &self,
    ) -> Option<(&u8, &Asb<S2, E, { s1a3(MIN_LEN) }>)> {
        match self.payloads.split_first() {
            Some((first, rest)) => {
                Some((first, Asb::<S2, E, { s1a3(MIN_LEN) }>::new_unchecked(rest)))
            }
            None => None,
        }
    }

    const unsafe fn try_split_first_raw_mut<S2: ~const State>(
        &mut self,
    ) -> Option<(&mut u8, &mut Asb<S2, E, { s1a3(MIN_LEN) }>)> {
        match self.payloads.split_first_mut() {
            Some((first, rest)) => Some((
                first,
                Asb::<S2, E, { s1a3(MIN_LEN) }>::new_unchecked_mut(rest),
            )),
            None => None,
        }
    }

    const unsafe fn try_split_last_raw<E2: ~const State>(
        &self,
    ) -> Option<(&u8, &Asb<S, E2, { s1a3(MIN_LEN) }>)> {
        match self.payloads.split_last() {
            Some((last, init)) => {
                Some((last, Asb::<S, E2, { s1a3(MIN_LEN) }>::new_unchecked(init)))
            }
            None => None,
        }
    }

    const unsafe fn try_split_last_raw_mut<E2: ~const State>(
        &mut self,
    ) -> Option<(&mut u8, &mut Asb<S, E2, { s1a3(MIN_LEN) }>)> {
        match self.payloads.split_last_mut() {
            Some((last, init)) => Some((
                last,
                Asb::<S, E2, { s1a3(MIN_LEN) }>::new_unchecked_mut(init),
            )),
            None => None,
        }
    }

    const unsafe fn split_first_raw<S2: ~const State>(
        &self,
    ) -> (&u8, &Asb<S2, E, { MIN_LEN - 1 }>) {
        match self.payloads.split_first() {
            Some((first, rest)) => (first, Asb::<S2, E, { MIN_LEN - 1 }>::new_unchecked(rest)),
            None => unreachable_unchecked(),
        }
    }

    const unsafe fn split_first_raw_mut<S2: ~const State>(
        &mut self,
    ) -> (&mut u8, &mut Asb<S2, E, { MIN_LEN - 1 }>) {
        match self.payloads.split_first_mut() {
            Some((first, rest)) => (
                first,
                Asb::<S2, E, { MIN_LEN - 1 }>::new_unchecked_mut(rest),
            ),
            None => unreachable_unchecked(),
        }
    }

    const unsafe fn split_last_raw<E2: ~const State>(&self) -> (&u8, &Asb<S, E2, { MIN_LEN - 1 }>) {
        match self.payloads.split_last() {
            Some((last, init)) => (last, Asb::<S, E2, { MIN_LEN - 1 }>::new_unchecked(init)),
            None => unreachable_unchecked(),
        }
    }

    const unsafe fn split_last_raw_mut<E2: ~const State>(
        &mut self,
    ) -> (&mut u8, &mut Asb<S, E2, { MIN_LEN - 1 }>) {
        match self.payloads.split_last_mut() {
            Some((last, init)) => (last, Asb::<S, E2, { MIN_LEN - 1 }>::new_unchecked_mut(init)),
            None => unreachable_unchecked(),
        }
    }
}

/// Methods for inspecting the first element of a borrowed assemblage.
impl<S: ~const Sss<E, MIN_LEN>, E: ~const State, const MIN_LEN: usize> Asb<S, E, MIN_LEN> {
    /// Tries to split an assemblage into its first element and the remainder.
    ///
    /// If the assemblage is empty, then this method returns [`None`].
    #[inline]
    pub const fn try_split_first(
        &self,
    ) -> Option<(&S::SPayload, &Asb<S::Successor, E, { s1a3(MIN_LEN) }>)> {
        unsafe {
            match self.try_split_first_raw::<S::Successor>() {
                Some((i, r)) => Some((mem::transmute(i), r)),
                None => None,
            }
        }
    }

    /// Splits an assemblage into its first element and the remainder.
    ///
    /// This method is applicable only when the assemblage is guaranteed to have only one element and thus never fails.
    #[inline]
    pub const fn split_first(&self) -> (&S::SPayload, &Asb<S::Successor, E, { MIN_LEN - 1 }>) {
        unsafe {
            let (i, r) = self.split_first_raw::<S::Successor>();
            (mem::transmute(i), r)
        }
    }

    /// Tries to split an assemblage into its first element and the remainder.
    ///
    /// If the assemblage is empty, then this method returns [`None`].
    #[inline]
    pub const fn try_split_first_mut(
        &mut self,
    ) -> Option<(&S::SPayload, &mut Asb<S::Successor, E, { s1a3(MIN_LEN) }>)> {
        unsafe {
            match self.try_split_first_raw_mut::<S::Successor>() {
                Some((i, r)) => Some((mem::transmute(i), r)),
                None => None,
            }
        }
    }

    /// Splits an assemblage into its first element and the remainder.
    ///
    /// This method is applicable only when the assemblage is guaranteed to have only one element and thus never fails.
    #[inline]
    pub const fn split_first_mut(
        &mut self,
    ) -> (&S::SPayload, &mut Asb<S::Successor, E, { MIN_LEN - 1 }>) {
        unsafe {
            let (i, r) = self.split_first_raw_mut::<S::Successor>();
            (mem::transmute(i), r)
        }
    }
}

/// An enum returned by [`Asb::tsplit_first`].
pub enum NucSplitFirstToTerm<'a> {
    /// The next state is the terminal state, and thus there are no more elements.
    Trm(&'a TerminalCoda),
    /// The next state is the syllabic state, and thus there are at least 4 more elements left in the assemblage.
    Syl(&'a Coda, &'a Asb<Syl, Trm, 4>),
}
/// An enum returned by [`Asb::tsplit_first_mut`].
pub enum NucSplitFirstToTermMut<'a> {
    /// The next state is the terminal state, and thus there are no more elements.
    Trm(&'a mut TerminalCoda),
    /// The next state is the syllabic state, and thus there are at least 4 more elements left in the assemblage.
    Syl(&'a mut Coda, &'a Asb<Syl, Trm, 4>),
}

/// Methods for inspecting the first element of a borrowed nuclear-to-terminal assemblage, where the next state may either be syllabic or terminal.
///
/// This is possible only when `MIN_LEN <= 1`, but the implementation is available for all values of `MIN_LEN` to facilitate generic code.
///
/// These methods are sound even when `MIN_LEN == 0`, as a nuclear-to-terminal assemblage must have at least one element.
impl<const MIN_LEN: usize> Asb<Nuc, Trm, MIN_LEN> {
    /// Splits an assemblage into its first element and the remainder.
    ///
    /// If an assemblage from a nuclear to a terminal state is only guaranteed to have a length of at least one, then the next state might be either the syllabic or the terminal state. Thus, this method returns an enum declaring which possibility applies.
    pub const fn tsplit_first(&self) -> NucSplitFirstToTerm {
        if self.len() == 1 {
            unsafe { NucSplitFirstToTerm::Trm(mem::transmute(&self.payloads[0])) }
        } else {
            unsafe {
                NucSplitFirstToTerm::Syl(
                    mem::transmute(&self.payloads[0]),
                    mem::transmute(self.payloads.split_first().unwrap_unchecked().1),
                )
            }
        }
    }

    /// Splits an assemblage into its first element and the remainder.
    ///
    /// If an assemblage from a nuclear to a terminal state is only guaranteed to have a length of at least one, then the next state might be either the syllabic or the terminal state. Thus, this method returns an enum declaring which possibility applies.
    pub const fn tsplit_first_mut(&mut self) -> NucSplitFirstToTermMut {
        if self.len() == 1 {
            unsafe { NucSplitFirstToTermMut::Trm(mem::transmute(&mut self.payloads[0])) }
        } else {
            unsafe {
                NucSplitFirstToTermMut::Syl(
                    mem::transmute(&mut self.payloads[0]),
                    mem::transmute(self.payloads.split_first_mut().unwrap_unchecked().1),
                )
            }
        }
    }
}

/// Methods for inspecting the last element of a borrowed assemblage.
impl<S: ~const State, E: ~const State, const MIN_LEN: usize> Asb<S, E, MIN_LEN> {
    /// Tries to split an assemblage into its last element and the previous elements.
    ///
    /// If the assemblage is empty, then this method returns [`None`].
    #[inline]
    pub const fn try_split_last(
        &self,
    ) -> Option<(E::PPayload, &Asb<S, E::Predecessor, { s1a3(MIN_LEN) }>)> {
        unsafe {
            match self.try_split_last_raw::<E::Predecessor>() {
                Some((i, r)) => Some((mem::transmute_copy(&i), r)),
                None => None,
            }
        }
    }

    /// Splits an assemblage into its last element and the previous elements.
    ///
    /// This method is applicable only when the assemblage is guaranteed to have only one element and thus never fails.
    #[inline]
    pub const fn split_last(&self) -> (E::PPayload, &Asb<S, E::Predecessor, { MIN_LEN - 1 }>) {
        unsafe {
            let (i, r) = self.split_last_raw::<E::Predecessor>();
            (mem::transmute_copy(&i), r)
        }
    }

    /// Tries to split an assemblage into its last element and the previous elements.
    ///
    /// If the assemblage is empty, then this method returns [`None`].
    #[inline]
    pub const fn try_split_last_mut(
        &mut self,
    ) -> Option<(E::PPayload, &mut Asb<S, E::Predecessor, { s1a3(MIN_LEN) }>)> {
        unsafe {
            match self.try_split_last_raw_mut::<E::Predecessor>() {
                Some((i, r)) => Some((mem::transmute_copy(&i), r)),
                None => None,
            }
        }
    }

    /// Splits an assemblage into its last element and the previous elements.
    ///
    /// This method is applicable only when the assemblage is guaranteed to have only one element and thus never fails.
    #[inline]
    pub const fn split_last_mut(
        &mut self,
    ) -> (E::PPayload, &mut Asb<S, E::Predecessor, { MIN_LEN - 1 }>) {
        unsafe {
            let (i, r) = self.split_last_raw_mut::<E::Predecessor>();
            (mem::transmute_copy(&i), r)
        }
    }
}

impl<S: ~const State, E: ~const State, const MIN_LEN: usize> Assemblage<S, E, MIN_LEN> {
    /// Converts a byte vector to an owned assemblage.
    ///
    /// # Safety
    ///
    /// * `vec` must be at least `MIN_LEN` elements long.
    /// * `vec` must contain valid data given the start and end states.
    #[inline]
    pub const unsafe fn new_unchecked(vec: SmallVec<[u8; ASMSVSZ]>) -> Self {
        Self {
            _phantom: PhantomData,
            payloads: vec,
        }
    }

    /// Returns the raw byte representation of this assemblage.
    #[inline]
    pub fn into_raw(self) -> SmallVec<[u8; ASMSVSZ]> {
        self.payloads
    }

    /// Constructs an owned [`Assemblage`] from a reference to [`Asb`].
    pub fn from_slice(slice: &Asb<S, E, MIN_LEN>) -> Self {
        unsafe { Self::new_unchecked(SmallVec::from_slice(&slice.payloads)) }
    }

    unsafe fn push_raw<E2: State>(mut self, e: E2::PPayload) -> Assemblage<S, E2, { MIN_LEN + 1 }> {
        self.payloads.push(mem::transmute_copy(&e));
        Assemblage::new_unchecked(self.payloads)
    }

    unsafe fn pop_raw<E2: State>(mut self) -> (u8, Assemblage<S, E2, { MIN_LEN - 1 }>) {
        let x = self.payloads.pop().unwrap_unchecked();
        (x, Assemblage::new_unchecked(self.payloads))
    }

    unsafe fn try_pop_raw<E2: State>(
        mut self,
    ) -> Option<(u8, Assemblage<S, E2, { s1a3(MIN_LEN) }>)> {
        match self.payloads.pop() {
            Some(x) => Some((x, Assemblage::new_unchecked(self.payloads))),
            None => None,
        }
    }
}

/// Methods for modifying an owned assemblage.
impl<S: State, E: State, const MIN_LEN: usize> Assemblage<S, E, MIN_LEN> {
    /// Given an assemblage and a payload, appends a path to the next state with that payload.
    ///
    /// # Examples
    ///
    /// ```
    /// # use ps9_core::sequence::asb::*;
    /// # use ps9_core::segment::onset::*;
    /// # use ps9_core::segment::cons::*;
    /// # use ps9_core::segment::vowel::*;
    /// # use ps9_core::segment::coda::*;
    /// #
    /// // Construct ⟦flirora⟧
    ///
    /// let _asb: Assemblage::<Syl, Trm, 12> = Assemblage::<Syl, _, 0>::new()
    ///     .push(ExpandedInitial::Cl(Epf::F, Liquid::L).compact())
    ///     .push(Medial::Empty)
    ///     .push(Vowel::I)
    ///     .push(Coda::Empty)
    ///     .push(ExpandedInitial::C(Consonant::R).compact())
    ///     .push(Medial::Empty)
    ///     .push(Vowel::O)
    ///     .push(Coda::Empty)
    ///     .push(ExpandedInitial::C(Consonant::R).compact())
    ///     .push(Medial::Empty)
    ///     .push(Vowel::A)
    ///     .push(TerminalCoda::Empty);
    ///
    /// ```
    #[inline]
    #[must_use = "this method does not modify the assemblage"]
    pub fn push<P: Payload<Predecessor = E>>(
        self,
        e: P,
    ) -> Assemblage<S, P::Successor, { MIN_LEN + 1 }> {
        unsafe { self.push_raw(e) }
    }

    /// Tries to remove the last element of an assemblage, returning it with the remainder.
    ///
    /// If the assemblage is empty, then this method returns [`None`].
    #[inline]
    pub fn try_pop(
        self,
    ) -> Option<(
        E::PPayload,
        Assemblage<S, E::Predecessor, { s1a3(MIN_LEN) }>,
    )> {
        unsafe {
            self.try_pop_raw::<E::Predecessor>()
                .map(|(i, r)| (mem::transmute_copy(&i), r))
        }
    }

    /// Removes the last element of an assemblage, returning it with the remainder.
    ///
    /// This method is applicable only when the assemblage is guaranteed to have only one element and thus never fails.
    #[inline]
    pub fn pop(self) -> (E::PPayload, Assemblage<S, E::Predecessor, { MIN_LEN - 1 }>) {
        unsafe {
            let (i, r) = self.pop_raw::<E::Predecessor>();
            (mem::transmute_copy(&i), r)
        }
    }
}

impl<S: ~const State> Assemblage<S, S, 0> {
    /// Creates an empty assemblage.
    #[inline]
    pub const fn new() -> Self {
        unsafe { Self::new_unchecked(SmallVec::new_const()) }
    }
}

impl<S: ~const State> const Default for Assemblage<S, S, 0> {
    #[inline]
    fn default() -> Self {
        Self::new()
    }
}

impl<S: State, E: State, const MIN_LEN: usize> Deref for Assemblage<S, E, MIN_LEN> {
    type Target = Asb<S, E, MIN_LEN>;

    #[inline]
    fn deref(&self) -> &Self::Target {
        unsafe { mem::transmute::<&[u8], _>(&self.payloads[..]) }
    }
}

impl<S: State, E: State, const MIN_LEN: usize> DerefMut for Assemblage<S, E, MIN_LEN> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { mem::transmute::<&mut [u8], _>(&mut self.payloads[..]) }
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> AsRef<Asb<S, E, MIN_LEN_2>>
    for Assemblage<S, E, MIN_LEN>
where
    [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
{
    #[inline]
    fn as_ref(&self) -> &Asb<S, E, MIN_LEN_2> {
        self.deref().weaken()
    }
}

impl<S: ~const State, E: ~const State, const MIN_LEN: usize, const MIN_LEN_2: usize> const
    AsRef<Asb<S, E, MIN_LEN_2>> for Asb<S, E, MIN_LEN>
where
    [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
{
    #[inline]
    fn as_ref(&self) -> &Asb<S, E, MIN_LEN_2> {
        self.weaken()
    }
}

impl<S: ~const State, E: ~const State, const MIN_LEN: usize, const MIN_LEN_2: usize>
    AsMut<Asb<S, E, MIN_LEN_2>> for Assemblage<S, E, MIN_LEN>
where
    [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
{
    #[inline]
    fn as_mut(&mut self) -> &mut Asb<S, E, MIN_LEN_2> {
        self.deref_mut().weaken_mut()
    }
}

impl<S: ~const State, E: ~const State, const MIN_LEN: usize, const MIN_LEN_2: usize> const
    AsMut<Asb<S, E, MIN_LEN_2>> for Asb<S, E, MIN_LEN>
where
    [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
{
    #[inline]
    fn as_mut(&mut self) -> &mut Asb<S, E, MIN_LEN_2> {
        self.weaken_mut()
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> Borrow<Asb<S, E, MIN_LEN_2>>
    for Assemblage<S, E, MIN_LEN>
where
    [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
{
    #[inline]
    fn borrow(&self) -> &Asb<S, E, MIN_LEN_2> {
        self.deref().weaken()
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize>
    BorrowMut<Asb<S, E, MIN_LEN_2>> for Assemblage<S, E, MIN_LEN>
where
    [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
{
    #[inline]
    fn borrow_mut(&mut self) -> &mut Asb<S, E, MIN_LEN_2> {
        self.deref_mut().weaken_mut()
    }
}

#[allow(clippy::eq_op)]
impl<S: State, E: State, const MIN_LEN: usize> ToOwned for Asb<S, E, MIN_LEN>
where
    // This is always true, but rustc isn’t smart enough to know that.
    [(); (MIN_LEN >= MIN_LEN) as usize - 1]:,
{
    type Owned = Assemblage<S, E, MIN_LEN>;

    #[inline]
    fn to_owned(&self) -> Self::Owned {
        Assemblage::from_slice(self)
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> From<&Asb<S, E, MIN_LEN>>
    for Assemblage<S, E, MIN_LEN_2>
where
    [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
{
    #[inline]
    fn from(value: &Asb<S, E, MIN_LEN>) -> Self {
        Assemblage::from_slice(value).weaken()
    }
}

impl<S: ~const State, E: ~const State, const MIN_LEN: usize> Asb<S, E, MIN_LEN> {
    /// Relaxes the minimum length guarantee for a shared assemblage slice.
    #[inline]
    pub const fn weaken<const MIN_LEN_2: usize>(&self) -> &Asb<S, E, MIN_LEN_2>
    where
        [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
    {
        unsafe { mem::transmute(self) }
    }

    /// Relaxes the minimum length guarantee for a mutable assemblage slice.
    #[inline]
    pub const fn weaken_mut<const MIN_LEN_2: usize>(&mut self) -> &mut Asb<S, E, MIN_LEN_2>
    where
        [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
    {
        unsafe { mem::transmute(self) }
    }

    /// Tries to strengthen the minimum length guarantee for a shared assemblage slice.
    ///
    /// If the assemblage is less than the requested length, then this method will return a [`TooShort`] error.
    #[inline]
    pub const fn strengthen<const MIN_LEN_2: usize>(
        &self,
    ) -> Result<&Asb<S, E, MIN_LEN_2>, TooShort> {
        if self.len() >= MIN_LEN_2 {
            unsafe { Ok(mem::transmute(self)) }
        } else {
            Err(TooShort {
                required_len: MIN_LEN_2,
                found_len: self.len(),
            })
        }
    }

    /// Tries to strengthen the minimum length guarantee for a mutable assemblage slice.
    ///
    /// If the assemblage is less than the requested length, then this method will return a [`TooShort`] error.
    #[inline]
    pub const fn strengthen_mut<const MIN_LEN_2: usize>(
        &mut self,
    ) -> Result<&mut Asb<S, E, MIN_LEN_2>, TooShort> {
        if self.len() >= MIN_LEN_2 {
            unsafe { Ok(mem::transmute(self)) }
        } else {
            Err(TooShort {
                required_len: MIN_LEN_2,
                found_len: self.len(),
            })
        }
    }
}

impl<S: State, E: State, const MIN_LEN: usize> Assemblage<S, E, MIN_LEN> {
    /// Relaxes the minimum length guarantee for an owned assemblage.
    #[inline]
    pub fn weaken<const MIN_LEN_2: usize>(self) -> Assemblage<S, E, MIN_LEN_2>
    where
        [(); (MIN_LEN >= MIN_LEN_2) as usize - 1]:,
    {
        unsafe { Assemblage::new_unchecked(self.payloads) }
    }

    /// Tries to strengthen the minimum length guarantee for an owned assemblage.
    ///
    /// If the assemblage is less than the requested length, then this method will return a [`TooShort`] error.
    #[inline]
    pub fn strengthen<const MIN_LEN_2: usize>(
        self,
    ) -> Result<Assemblage<S, E, MIN_LEN_2>, TooShort> {
        if self.len() >= MIN_LEN_2 {
            unsafe { Ok(Assemblage::new_unchecked(self.payloads)) }
        } else {
            Err(TooShort {
                required_len: MIN_LEN_2,
                found_len: self.len(),
            })
        }
    }
}

/*
// The following should be implemented eventually, but this currently fails to compile because of the conflicts with reflexive implementations for the same traits. It seems that rustc isn’t yet smart enough to conclude that `MIN_LEN` and `MIN_LEN_2` are different for all such implementations.
//
// See <https://rust-lang.zulipchat.com/#narrow/stream/260443-project-const-generics/topic/Requiring.20two.20consts.20to.20be.20unequal>

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> const
    From<&Asb<S, E, MIN_LEN>> for &Asb<S, E, MIN_LEN_2>
where
    [(); (MIN_LEN > MIN_LEN_2) as usize - 1]:,
{
    fn from(value: &Asb<S, E, MIN_LEN>) -> Self {
        self.weaken()
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> const
    From<&mut Asb<S, E, MIN_LEN>> for &mut Asb<S, E, MIN_LEN_2>
where
    [(); (MIN_LEN > MIN_LEN_2) as usize - 1]:,
{
    fn from(value: &mut Asb<S, E, MIN_LEN>) -> Self {
        self.weaken()
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> const
    From<Assemblage<S, E, MIN_LEN>> for Assemblage<S, E, MIN_LEN_2>
where
    [(); (MIN_LEN > MIN_LEN_2) as usize - 1]:,
{
    fn from(value: Assemblage<S, E, MIN_LEN>) -> Self {
        self.weaken()
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> const
    TryFrom<&Asb<S, E, MIN_LEN>> for &Asb<S, E, MIN_LEN_2>
where
    [(); (MIN_LEN != MIN_LEN_2) as usize - 1]:,
{
    type Error = TooShort;

    fn try_from(value: &Asb<S, E, MIN_LEN>) -> Result<Self, Self::Error> {
        self.strengthen()
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> const
    TryFrom<&mut Asb<S, E, MIN_LEN>> for &mut Asb<S, E, MIN_LEN_2>
where
    [(); (MIN_LEN != MIN_LEN_2) as usize - 1]:,
{
    type Error = TooShort;

    fn try_from(value: &mut Asb<S, E, MIN_LEN>) -> Result<Self, Self::Error> {
        self.strengthen()
    }
}

impl<S: State, E: State, const MIN_LEN: usize, const MIN_LEN_2: usize> const
    TryFrom<Assemblage<S, E, MIN_LEN>> for Assemblage<S, E, MIN_LEN_2>
where
    [(); (MIN_LEN != MIN_LEN_2) as usize - 1]:,
{
    type Error = TooShort;

    fn try_from(value: Assemblage<S, E, MIN_LEN>) -> Result<Self, Self::Error> {
        self.strengthen()
    }
}
*/

/// Describes what to do on encountering each type of payload while iterating through an assemblage.
///
/// A visitor may hold a piece of memory that varies by the current assemblage state. If you don’t need this, then you can use [`SimpleAsbVisitor`]. Otherwise, the memory for the state preceding the payload is passed into the appropriate function, which should return the memory for the following state.
///
/// In addition, a visitor can choose to stop the traversal early by returning a [`ControlFlow::Break`] variant in one or more of its methods.
pub trait AsbVisitor {
    /// The type of the state to pass while visiting an assemblage.
    type Memory<S: State>;
    /// The type of value to return when stopping the visit early.
    type Break = !;

    /// Called when an initial is visited.
    fn accept_initial(
        &mut self,
        value: Initial,
        mem: Self::Memory<Syl>,
    ) -> ControlFlow<Self::Break, Self::Memory<Gld>>;
    /// Called when a medial is visited.
    fn accept_medial(
        &mut self,
        value: Medial,
        mem: Self::Memory<Gld>,
    ) -> ControlFlow<Self::Break, Self::Memory<Ons>>;
    /// Called when a vowel is visited.
    fn accept_vowel(
        &mut self,
        value: Vowel,
        mem: Self::Memory<Ons>,
    ) -> ControlFlow<Self::Break, Self::Memory<Nuc>>;
    /// Called when a codad is visited.
    fn accept_coda(
        &mut self,
        value: Coda,
        mem: Self::Memory<Nuc>,
    ) -> ControlFlow<Self::Break, Self::Memory<Syl>>;
    /// Called when a terminal is visited.
    fn accept_terminal_coda(
        &mut self,
        value: TerminalCoda,
        mem: Self::Memory<Nuc>,
    ) -> ControlFlow<Self::Break, Self::Memory<Trm>>;
}

/// A variant of [`AsbVisitor`] that does not hold per-state memory.
pub trait SimpleAsbVisitor {
    /// The type of value to return when stopping the visit early.
    type Break = !;

    /// Called when an initial is visited.
    fn accept_initial(&mut self, value: Initial) -> ControlFlow<Self::Break>;
    /// Called when a medial is visited.
    fn accept_medial(&mut self, value: Medial) -> ControlFlow<Self::Break>;
    /// Called when a vowel is visited.
    fn accept_vowel(&mut self, value: Vowel) -> ControlFlow<Self::Break>;
    /// Called when a codad is visited.
    fn accept_coda(&mut self, value: Coda) -> ControlFlow<Self::Break>;
    /// Called when a terminal is visited.
    fn accept_terminal_coda(&mut self, value: TerminalCoda) -> ControlFlow<Self::Break>;
}

impl<T: SimpleAsbVisitor> AsbVisitor for T {
    type Memory<S: State> = ();
    type Break = <T as SimpleAsbVisitor>::Break;

    fn accept_initial(
        &mut self,
        value: Initial,
        _: Self::Memory<Syl>,
    ) -> ControlFlow<Self::Break, Self::Memory<Gld>> {
        self.accept_initial(value)
    }

    fn accept_medial(
        &mut self,
        value: Medial,
        _: Self::Memory<Gld>,
    ) -> ControlFlow<Self::Break, Self::Memory<Ons>> {
        self.accept_medial(value)
    }

    fn accept_vowel(
        &mut self,
        value: Vowel,
        _: Self::Memory<Ons>,
    ) -> ControlFlow<Self::Break, Self::Memory<Nuc>> {
        self.accept_vowel(value)
    }

    fn accept_coda(
        &mut self,
        value: Coda,
        _: Self::Memory<Nuc>,
    ) -> ControlFlow<Self::Break, Self::Memory<Syl>> {
        self.accept_coda(value)
    }

    fn accept_terminal_coda(
        &mut self,
        value: TerminalCoda,
        _: Self::Memory<Nuc>,
    ) -> ControlFlow<Self::Break, Self::Memory<Trm>> {
        self.accept_terminal_coda(value)
    }
}

struct VisitorWrapper<V: AsbVisitor>(V);
impl<S: State, V: AsbVisitor> TypeFunction<S> for VisitorWrapper<V> {
    type Result = V::Memory<S>;
}

type AsbVisitorMemory<V> = StateE<
    <V as AsbVisitor>::Memory<Syl>,
    <V as AsbVisitor>::Memory<Gld>,
    <V as AsbVisitor>::Memory<Ons>,
    <V as AsbVisitor>::Memory<Nuc>,
    <V as AsbVisitor>::Memory<Trm>,
>;

fn tag_memory<V: AsbVisitor, S: State>(m: V::Memory<S>) -> AsbVisitorMemory<V> {
    S::select(
        |ev, m| ev.lift_through::<VisitorWrapper<V>>().coerce(m),
        |ev, m| ev.lift_through::<VisitorWrapper<V>>().coerce(m),
        |ev, m| ev.lift_through::<VisitorWrapper<V>>().coerce(m),
        |ev, m| ev.lift_through::<VisitorWrapper<V>>().coerce(m),
        |ev, m| ev.lift_through::<VisitorWrapper<V>>().coerce(m),
        m,
    )
}

impl<S: State, E: State, const MIN_LEN: usize> Asb<S, E, MIN_LEN> {
    /// Visits each segment of an assemblage using an [`AsbVisitor`].
    ///
    /// This method expects an initial memory and returns the final memory.
    pub fn visit<V: AsbVisitor>(
        &self,
        visitor: &mut V,
        initial_memory: V::Memory<S>,
    ) -> ControlFlow<V::Break, V::Memory<E>> {
        let mut memory = tag_memory::<V, S>(initial_memory);
        let mut underlying = &self.deref().payloads;
        while let Some((x, rest)) = underlying.split_first() {
            match memory {
                StateE::Syl(m) => {
                    // Syl ⇒ Gld, accepting an initial
                    let value = unsafe { mem::transmute(*x) };
                    let m = visitor.accept_initial(value, m)?;
                    memory = StateE::Gld(m);
                }
                StateE::Gld(m) => {
                    // Gld ⇒ Ons, accepting a medial
                    let value = unsafe { mem::transmute(*x) };
                    let m = visitor.accept_medial(value, m)?;
                    memory = StateE::Ons(m);
                }
                StateE::Ons(m) => {
                    // Ons ⇒ Nuc, accepting a vowel
                    let value = unsafe { mem::transmute(*x) };
                    let m = visitor.accept_vowel(value, m)?;
                    memory = StateE::Nuc(m);
                }
                StateE::Nuc(m) => {
                    // This can either transition to the syllabic or the terminal.
                    let transition_to_terminal = E::select(
                        |_, _| false,
                        |_, _| false,
                        |_, _| false,
                        |_, _| false,
                        |_, _| rest.is_empty(),
                        (),
                    )
                    .unify();
                    if transition_to_terminal {
                        // Nuc ⇒ Trm, accepting a terminal coda
                        let value = unsafe { mem::transmute(*x) };
                        let m = visitor.accept_terminal_coda(value, m)?;
                        memory = StateE::Trm(m);
                    } else {
                        // Nuc ⇒ Syl, accepting a coda
                        let value = unsafe { mem::transmute(*x) };
                        let m = visitor.accept_coda(value, m)?;
                        memory = StateE::Syl(m);
                    }
                }
                // The terminal state has no following states
                StateE::Trm(_) => unsafe { unreachable_unchecked() },
            }
            underlying = rest;
        }
        ControlFlow::Continue(match memory {
            StateE::Syl(m) => unsafe { utils::transmute_ignore_size(m) },
            StateE::Gld(m) => unsafe { utils::transmute_ignore_size(m) },
            StateE::Ons(m) => unsafe { utils::transmute_ignore_size(m) },
            StateE::Nuc(m) => unsafe { utils::transmute_ignore_size(m) },
            StateE::Trm(m) => unsafe { utils::transmute_ignore_size(m) },
        })
    }
}

struct DisplayVisitor<'a, 'b>(&'a mut std::fmt::Formatter<'b>);

impl<'a, 'b> SimpleAsbVisitor for DisplayVisitor<'a, 'b> {
    type Break = fmt::Error;

    fn accept_initial(&mut self, value: Initial) -> ControlFlow<Self::Break> {
        Display::fmt(&value, self.0).continue_on_ok()
    }

    fn accept_medial(&mut self, value: Medial) -> ControlFlow<Self::Break> {
        Display::fmt(&value, self.0).continue_on_ok()
    }

    fn accept_vowel(&mut self, value: Vowel) -> ControlFlow<Self::Break> {
        Display::fmt(&value, self.0).continue_on_ok()
    }

    fn accept_coda(&mut self, value: Coda) -> ControlFlow<Self::Break> {
        Display::fmt(&value, self.0).continue_on_ok()
    }

    fn accept_terminal_coda(&mut self, value: TerminalCoda) -> ControlFlow<Self::Break> {
        Display::fmt(&value, self.0).continue_on_ok()
    }
}

impl<S: State, E: State, const MIN_LEN: usize> Display for Asb<S, E, MIN_LEN> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut visitor = DisplayVisitor(f);
        self.visit(&mut visitor, ()).into_result()
    }
}

impl<S: State, E: State, const MIN_LEN: usize> Debug for Asb<S, E, MIN_LEN> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self.to_string(), f)
    }
}

impl<S: State, E: State, const MIN_LEN: usize> Display for Assemblage<S, E, MIN_LEN> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(self.deref(), f)
    }
}

impl<S: State, E: State, const MIN_LEN: usize> Debug for Assemblage<S, E, MIN_LEN> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(self.deref(), f)
    }
}
