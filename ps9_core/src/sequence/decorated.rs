//! A wrapper around a type that adds a [`Decoration`] to it.

use std::{
    fmt::{Display, Write},
    marker::Destruct,
};

use crate::segment::decoration::Decoration;

use super::{asb::Assemblage, fragment::Fragment};

/// A wrapper around a type that adds a [`Decoration`] to it.
///
/// Usually, this type will wrap an undecorated sequence of characters
/// such as a [`Fragment`] or an [`Assemblage`].
#[derive(Copy, Eq, Hash, Debug)]
#[derive_const(Clone, PartialEq, Default)]
#[cfg_attr(test, derive(proptest_derive::Arbitrary))]
pub struct Decorated<T> {
    /// The value being wrapped.
    pub content: T,
    /// The [`Decoration`] associated with this value.
    pub decoration: Decoration,
}

impl<T> Decorated<T> {
    /// Wraps a value with an empty decoration.
    pub const fn undecorated(content: T) -> Self {
        Decorated {
            content,
            decoration: Decoration::default(),
        }
    }

    /// Transforms the wrapped value using a closure.
    pub const fn map<U>(self, f: impl ~const FnOnce(T) -> U) -> Decorated<U>
    where
        T: ~const Destruct,
    {
        Decorated {
            content: f(self.content),
            decoration: self.decoration,
        }
    }
}

impl<T> const From<T> for Decorated<T> {
    fn from(value: T) -> Self {
        Decorated::undecorated(value)
    }
}

/// A word with an optional decoration, where the contents of the word are represented as a [`Fragment`].
pub type DecoratedWord = Decorated<Fragment>;

/// A word with an optional decoration, where the contents of the word are represented as an [`Assemblage`].
pub type DecoratedAssemblage<S, E, const MIN_LEN: usize> = Decorated<Assemblage<S, E, MIN_LEN>>;

impl Display for DecoratedWord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.content.split_first() {
            Some((m0, ms)) if m0.is_eclipsed() => {
                let mut m0_str = m0.as_str().chars();
                f.write_char(m0_str.next().unwrap())?;
                Display::fmt(&self.decoration, f)?;
                f.write_str(m0_str.as_str())?;
                for m in ms {
                    Display::fmt(m, f)?;
                }
                Ok(())
            }
            _ => {
                Display::fmt(&self.decoration, f)?;
                Display::fmt(&self.content, f)
            }
        }
    }
}
