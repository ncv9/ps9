//! Contains functionality for dealing with fragments.
//!
//! Fragments are sequences of [manifested grapheme phrases][Mgp];
//! unlike [assemblages][super::asb], they are not further structured.
//! They are used as intermediaries for parsing strings into assemblages.

use std::{
    borrow::{Borrow, BorrowMut},
    fmt::Display,
    mem,
    ops::{Deref, DerefMut},
};

use crate::segment::mgp::Mgp;
use smallvec::SmallVec;
use static_assert_macro::static_assert;

/// A borrowed fragment (akin to [`str`]).
#[repr(transparent)]
#[derive(PartialEq, Eq, Hash, Debug)]
pub struct Frag(pub [Mgp]);

/// An owned fragment (akin to [`String`]).
#[repr(transparent)]
#[derive(Clone, PartialEq, Eq, Hash, Debug, Default)]
#[cfg_attr(test, derive(proptest_derive::Arbitrary))]
pub struct Fragment(
    #[cfg_attr(
        test,
        proptest(
            strategy = "proptest::prelude::Strategy::prop_map(proptest::prelude::any::<Vec<Mgp>>(), SmallVec::from)"
        )
    )]
    pub SmallVec<[Mgp; 16]>,
);

#[cfg(target_arch = "x86_64")]
static_assert!(mem::size_of::<Fragment>() == 24);

impl Fragment {
    /// Creates an empty [`Fragment`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Adds an [`Mgp`] at the end of a fragment.
    pub fn push(&mut self, mgp: Mgp) {
        self.0.push(mgp)
    }
}

impl Frag {
    /// Wraps a slice reference of [`Mgp`]s into a reference to a [`Frag`].
    pub fn from_slice(slice: &[Mgp]) -> &Self {
        unsafe { mem::transmute(slice) }
    }

    /// Wraps a mutable slice reference of [`Mgp`]s into a reference to a [`Frag`].
    pub fn from_slice_mut(slice: &mut [Mgp]) -> &mut Self {
        unsafe { mem::transmute(slice) }
    }
}

impl Deref for Fragment {
    type Target = Frag;

    fn deref(&self) -> &Self::Target {
        Frag::from_slice(&self.0)
    }
}

impl DerefMut for Fragment {
    fn deref_mut(&mut self) -> &mut Self::Target {
        Frag::from_slice_mut(&mut self.0)
    }
}

impl Deref for Frag {
    type Target = [Mgp];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Frag {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl AsRef<Frag> for Fragment {
    fn as_ref(&self) -> &Frag {
        self.deref()
    }
}

impl const AsRef<Frag> for Frag {
    fn as_ref(&self) -> &Frag {
        self
    }
}

impl AsMut<Frag> for Fragment {
    fn as_mut(&mut self) -> &mut Frag {
        self.deref_mut()
    }
}

impl const AsMut<Frag> for Frag {
    fn as_mut(&mut self) -> &mut Frag {
        self
    }
}

impl Borrow<Frag> for Fragment {
    fn borrow(&self) -> &Frag {
        self.deref()
    }
}

impl BorrowMut<Frag> for Fragment {
    fn borrow_mut(&mut self) -> &mut Frag {
        self.deref_mut()
    }
}

impl ToOwned for Frag {
    type Owned = Fragment;

    fn to_owned(&self) -> Self::Owned {
        Fragment(SmallVec::from_slice(&self.0))
    }
}

impl Display for Frag {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for m in &self.0 {
            Mgp::fmt(m, f)?;
        }
        Ok(())
    }
}

impl Display for Fragment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Frag::fmt(self.deref(), f)
    }
}
