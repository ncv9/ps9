//! The core library for ps9, the *P*roject *S*hiva experiments for Ŋarâþ Crîþ v*9*.

#![allow(incomplete_features)]
#![warn(missing_docs)]
// #![warn(clippy::missing_const_for_fn)]
// I hereby nominate ps9 as a guinea pig for const trait impls.
#![feature(const_trait_impl)]
#![feature(generic_const_exprs)]
#![feature(negative_impls)]
#![feature(associated_type_defaults)]
#![feature(const_mut_refs)]
#![feature(const_transmute_copy)]
#![feature(const_slice_first_last)]
#![feature(const_option_ext)]
#![feature(derive_const)]
#![feature(never_type)]
#![feature(const_discriminant)]
#![feature(const_cmp)]
#![feature(const_default_impls)]
#![feature(const_clone)]
#![feature(let_chains)]

pub mod control;
pub mod conv;
pub mod parse;
pub mod segment;
pub mod sequence;

#[cfg(test)]
mod tests {
    use std::ops::Deref;

    use crate::{
        parse::{dw::parse_dw, error::ParseError},
        segment::{
            coda::{Coda, TerminalCoda},
            cons::{Consonant, Epf, Liquid},
            decoration::{Decoration, PreMarker},
            mgp::Mgp,
            onset::{ExpandedInitial, Medial},
            vowel::Vowel,
        },
        sequence::{
            asb::{Assemblage, Syl, Trm},
            decorated::{Decorated, DecoratedWord},
            fragment::Fragment,
        },
    };
    use smallvec::smallvec;

    fn flirora() -> Assemblage<Syl, Trm, 12> {
        Assemblage::<Syl, _, 0>::new()
            .push(ExpandedInitial::Cl(Epf::F, Liquid::L).compact())
            .push(Medial::Empty)
            .push(Vowel::I)
            .push(Coda::Empty)
            .push(ExpandedInitial::C(Consonant::R).compact())
            .push(Medial::Empty)
            .push(Vowel::O)
            .push(Coda::Empty)
            .push(ExpandedInitial::C(Consonant::R).compact())
            .push(Medial::Empty)
            .push(Vowel::A)
            .push(TerminalCoda::Empty)
    }

    #[test]
    fn test_assemblage_construction() {
        let asb = flirora();
        let asb2 = asb.clone();
        // We’ll eventually have Debug and Display for assemblages
        assert_eq!(asb, asb2);
        assert_eq!(asb.deref(), asb2.deref());
        assert_eq!(&asb.to_string(), "flirora");
    }

    #[test]
    fn test_assemblage_push_pop() {
        let asb = flirora();
        // Let’s decline this in the genitive case
        let (term, asb) = asb.pop();
        assert_eq!(term, TerminalCoda::Empty);
        let (vow, asb) = asb.pop();
        assert_eq!(vow, Vowel::A);
        let asb = asb.push(Vowel::E);
        let asb = asb.push(TerminalCoda::N);
        assert_ne!(asb, flirora());
        assert_eq!(&asb.to_string(), "fliroren");
    }

    #[test]
    fn test_parse_flirora_dw() -> Result<(), ParseError> {
        let fl = Fragment(smallvec![
            Mgp::F,
            Mgp::L,
            Mgp::I,
            Mgp::R,
            Mgp::O,
            Mgp::R,
            Mgp::A
        ]);
        let jellyfish = parse_dw("flirora")?;
        assert_eq!(jellyfish, DecoratedWord::undecorated(fl.clone()));
        let est = parse_dw("#flirora")?;
        assert_eq!(
            est,
            Decorated {
                content: fl,
                decoration: Decoration {
                    premarker: PreMarker::Carþ,
                    has_nef: false,
                    has_sen: false
                }
            }
        );
        Ok(())
    }

    #[test]
    fn test_parse_lenited() -> Result<(), ParseError> {
        let word1 = parse_dw("f·g·")?;
        assert!(word1.decoration.is_empty());
        assert_eq!(&word1.content[..], &[Mgp::F·, Mgp::G·]);
        Ok(())
    }

    #[test]
    fn test_parse_eclipsed_initial() -> Result<(), ParseError> {
        let word1 = parse_dw("gcjani")?;
        assert!(word1.decoration.is_empty());
        assert_eq!(
            &word1.content[..],
            &[Mgp::Gc, Mgp::J, Mgp::A, Mgp::N, Mgp::I]
        );
        let word2 = parse_dw("g*corta")?;
        assert!(word2.decoration.has_nef);
        assert_eq!(
            &word2.content[..],
            &[Mgp::Gc, Mgp::O, Mgp::R, Mgp::T, Mgp::A]
        );
        let word3 = parse_dw("g@asoren")?;
        assert_eq!(word3.decoration.premarker, PreMarker::Es);
        assert_eq!(
            &word3.content[..],
            &[Mgp::G_, Mgp::A, Mgp::S, Mgp::O, Mgp::R, Mgp::E, Mgp::N]
        );
        Ok(())
    }

    #[test]
    fn test_parse_pathological() -> Result<(), ParseError> {
        let word1 = parse_dw("vf·")?;
        assert!(word1.decoration.is_empty());
        assert_eq!(&word1.content[..], &[Mgp::V, Mgp::F·]);
        Ok(())
    }

    use proptest::prelude::*;

    proptest! {
        #[test]
        fn parse_and_stringify_are_almost_reflexive(word in any::<DecoratedWord>()) {
            let s = word.to_string();
            let parsed = parse_dw(&s).unwrap_or_else(|_| panic!("could not parse string: {s}"));
            let stringified = parsed.to_string();
            assert_eq!(s, stringified);
        }
    }
}
