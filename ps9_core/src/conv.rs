//! Functionality related to conversions.

use std::marker::PhantomData;

use thiserror::Error;

/// Error returned when a conversion from a set of segments to a subset thereof fails.
#[derive(Error, Debug, Clone, Copy)]
#[error("conversion failed: {value}")]
pub struct ConvError<T, U>
where
    U: TryFrom<T>,
{
    /// The value that was attempted to be converted.
    pub value: T,
    _phantom: PhantomData<U>,
}

impl<T, U> ConvError<T, U>
where
    U: TryFrom<T>,
{
    /// Creates a new [`ConvError`].
    ///
    /// # Arguments
    ///
    /// * `value` – the value that was attempted to be converted
    pub const fn new(value: T) -> Self {
        Self {
            value,
            _phantom: PhantomData,
        }
    }
}

/// Error returned when a given sequence was shorter than required.
#[derive(Error, Debug, Clone, Copy)]
#[error("not enough elements: expected {required_len} but found {found_len}")]
pub struct TooShort {
    /// The number of elements that were required.
    pub required_len: usize,
    /// The number of elements that were actually in the sequence.
    pub found_len: usize,
}
