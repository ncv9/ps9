//! A struct for reading from a string.
//!
//! See [`StrReader`] for more details.

/// A struct for reading from a string.
#[derive(Clone, Debug)]
pub struct StrReader<'a> {
    /// The entire string that is being parsed.
    pub s: &'a str,
    /// The position that the reader is at.
    pub pos: usize,
}

impl<'a> StrReader<'a> {
    /// Creates a new [`StrReader`] from a string.
    pub fn from(s: &'a str) -> Self {
        StrReader { s, pos: 0 }
    }

    /// Returns the part of the string that still needs to be parsed.
    pub fn remainder(&self) -> &'a str {
        &self.s[self.pos..]
    }

    /// Returns true if there is nothing else to parse.
    pub fn is_empty(&self) -> bool {
        self.remainder().is_empty()
    }

    /// Advances the reader if the next character meets a certain predicate, returning it if this is the case.
    pub fn next_if(&mut self, pred: impl FnOnce(char) -> bool) -> Option<char> {
        let mut s = self.remainder().chars();
        match s.next() {
            None => None,
            Some(c) => {
                if pred(c) {
                    self.pos += c.len_utf8();
                    Some(c)
                } else {
                    None
                }
            }
        }
    }
}
