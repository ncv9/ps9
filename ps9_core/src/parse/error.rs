//! Errors raised during parsing.

use std::borrow::Cow;

use thiserror::Error;

/// An error raised during parsing.
#[derive(Error, Debug, Clone)]
#[non_exhaustive]
pub enum ParseError {
    /// The parser encountered an unexpected character.
    #[error("expected {expected}; got {actual} at {pos}")]
    Unexpected {
        /// The description of what was expected.
        expected: Cow<'static, str>,
        /// The description of what was actually encountered.
        actual: Cow<'static, str>,
        /// The position at which the parser encountered the unexpected character.
        pos: usize,
    },
}
