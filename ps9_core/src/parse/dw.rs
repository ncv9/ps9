//! Parsing [`DecoratedWord`]s from strings.

use crate::{
    segment::{
        decoration::{Decoration, PreMarker},
        mgp::Mgp,
    },
    sequence::{
        decorated::{Decorated, DecoratedWord},
        fragment::Fragment,
    },
};

use super::{error::ParseError, reader::StrReader};

impl<'a> StrReader<'a> {
    /// Parses a decoration from the reader.
    ///
    /// This always succeeds as an empty decoration is valid.
    pub fn parse_decoration(&mut self) -> Decoration {
        let has_nef = self.next_if(|c| c == '*').is_some();
        let premarker = match self.next_if(|c| "#+×@".contains(c)) {
            Some('#') => PreMarker::Carþ,
            Some('+') => match self.next_if(|c| c == '*') {
                Some(_) => PreMarker::Njor,
                None => PreMarker::Tor,
            },
            Some('×') => PreMarker::Njor,
            Some('@') => PreMarker::Es,
            _ => PreMarker::None,
        };
        let has_sen = self.next_if(|c| c == '&').is_some();
        Decoration {
            premarker,
            has_nef,
            has_sen,
        }
    }

    /// Parses an [`Mgp`] from the reader.
    ///
    /// If parsing fails, then the reader’s position is reset to what it was before the call.
    pub fn parse_mgp(&mut self, init: bool, expected: &'static str) -> Result<Mgp, ParseError> {
        match Mgp::parse(self.remainder(), init) {
            Some((m, s)) => {
                self.pos = self.s.len() - s.len();
                Ok(m)
            }
            None => Err(ParseError::Unexpected {
                expected: expected.into(),
                actual: "a non-MGP character".into(),
                pos: self.pos,
            }),
        }
    }

    /// Parses an [`Mgp`] from the reader and checks if it meets a certain predicate.
    ///
    /// If the predicate is not met, then the reader’s position is reset to what it was before the call.
    pub fn parse_mgp_expecting(
        &mut self,
        pred: impl FnOnce(Mgp) -> bool,
        expected: &'static str,
    ) -> Result<Mgp, ParseError> {
        let pos = self.pos;
        let m = self.parse_mgp(false, expected)?;
        if pred(m) {
            Ok(m)
        } else {
            self.pos = pos;
            Err(ParseError::Unexpected {
                expected: expected.into(),
                actual: m.as_str().into(),
                pos: self.pos,
            })
        }
    }

    /// Parses a [`DecoratedWord`] from the reader.
    ///
    /// If parsing fails, then the reader’s position is **not** reset.
    pub fn parse_dw(&mut self) -> Result<DecoratedWord, ParseError> {
        let mut frag = Fragment::default();
        let m0 = self.parse_mgp(true, "an MGP");
        let decoration = self.parse_decoration();
        if let Ok(m0) = m0 {
            let m0 = if decoration.is_empty() {
                m0
            } else {
                match m0 {
                    Mgp::M => {
                        self.parse_mgp_expecting(|c| c == Mgp::P, "p")?;
                        Mgp::Mp
                    }
                    Mgp::V => {
                        let m1 =
                            self.parse_mgp_expecting(|c| c == Mgp::P || c == Mgp::F, "v or f")?;
                        match m1 {
                            Mgp::P => Mgp::Vp,
                            Mgp::F => Mgp::Vf,
                            _ => unreachable!(),
                        }
                    }
                    Mgp::D => {
                        self.parse_mgp_expecting(|c| c == Mgp::T, "t")?;
                        Mgp::Dt
                    }
                    Mgp::N => {
                        self.parse_mgp_expecting(|c| c == Mgp::D, "d")?;
                        Mgp::Nd
                    }
                    Mgp::G => {
                        if self.parse_mgp_expecting(|c| c == Mgp::C, "c").is_ok() {
                            Mgp::Gc
                        } else {
                            Mgp::G_
                        }
                    }
                    Mgp::Ŋ => {
                        self.parse_mgp_expecting(|c| c == Mgp::G, "g")?;
                        Mgp::Ŋg
                    }
                    Mgp::Ð => {
                        self.parse_mgp_expecting(|c| c == Mgp::Þ, "þ")?;
                        Mgp::Ðþ
                    }
                    Mgp::L => {
                        self.parse_mgp_expecting(|c| c == Mgp::Ł, "ł")?;
                        Mgp::Lł
                    }
                    _ => {
                        return Err(ParseError::Unexpected {
                            expected: "an MGP".into(),
                            actual: format!("the marker(s) {decoration}").into(),
                            pos: self.pos,
                        })
                    }
                }
            };
            frag.push(m0);
        }
        while !self.is_empty() {
            let m = self.parse_mgp(false, "an MGP")?;
            frag.push(m);
        }
        Ok(Decorated {
            content: frag,
            decoration,
        })
    }
}

/// Parses a string into a [`DecoratedWord`].
///
/// # Errors
///
/// This function will return an error if the input is not a valid decorated word.
///
/// ```
/// # use ps9_core::parse::dw::parse_dw;
/// # use ps9_core::parse::error::ParseError;
/// # use ps9_core::segment::mgp::Mgp;
/// # use ps9_core::sequence::decorated::DecoratedWord;
/// # use ps9_core::sequence::fragment::Fragment;
/// # use smallvec::smallvec;
/// #
/// let jellyfish = parse_dw("flirora")?;
/// assert_eq!(jellyfish, DecoratedWord::undecorated(Fragment(smallvec![
///     Mgp::F,
///     Mgp::L,
///     Mgp::I,
///     Mgp::R,
///     Mgp::O,
///     Mgp::R,
///     Mgp::A
/// ])));
/// # Ok::<(), ParseError>(())
/// ```
pub fn parse_dw(s: &str) -> Result<DecoratedWord, ParseError> {
    let mut reader = StrReader::from(s);
    let result = reader.parse_dw()?;
    if !reader.is_empty() {
        return Err(ParseError::Unexpected {
            expected: "an MGP".into(),
            actual: reader.remainder().to_owned().into(),
            pos: reader.pos,
        });
    }
    Ok(result)
}
