//! Utilities for working with [`ControlFlow`].

use std::{marker::Destruct, ops::ControlFlow};

/// Extra methods for [`Result`].
#[const_trait]
pub trait ResultExt<T, E> {
    /// Turns [`Ok`] into [`ControlFlow::Continue`] and [`Err`] into [`ControlFlow::Break`].
    fn continue_on_ok(self) -> ControlFlow<E, T>
    where
        T: ~const Destruct,
        E: ~const Destruct;
}

impl<T, E> const ResultExt<T, E> for Result<T, E> {
    fn continue_on_ok(self) -> ControlFlow<E, T>
    where
        T: ~const Destruct,
        E: ~const Destruct,
    {
        match self {
            Ok(t) => ControlFlow::Continue(t),
            Err(e) => ControlFlow::Break(e),
        }
    }
}

/// Extra methods for [`ControlFlow`].
#[const_trait]
pub trait ControlFlowExt<B, C = ()> {
    /// Turns [`ControlFlow::Continue`] into [`Ok`] and [`ControlFlow::Break`] into [`Err`].
    fn into_result(self) -> Result<C, B>
    where
        B: ~const Destruct,
        C: ~const Destruct;
}

impl<B, C> const ControlFlowExt<B, C> for ControlFlow<B, C>
where
    B: ~const Destruct,
    C: ~const Destruct,
{
    fn into_result(self) -> Result<C, B> {
        match self {
            ControlFlow::Continue(c) => Ok(c),
            ControlFlow::Break(b) => Err(b),
        }
    }
}
