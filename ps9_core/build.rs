use std::{
    cmp::Reverse,
    collections::HashMap,
    env,
    fs::File,
    hash::Hash,
    io::{BufRead, BufReader, Write},
    str::FromStr,
};

use anyhow::{bail, Context};
use proc_macro2::{Ident, TokenStream};
use quote::{format_ident, quote};
use rust_format::{Formatter, RustFmt};

fn rev<T, K: Hash + Eq>(
    items: impl IntoIterator<Item = T>,
    mut key: impl FnMut(&T) -> K,
) -> HashMap<K, T> {
    items.into_iter().map(|item| (key(&item), item)).collect()
}

fn titlecase(s: &str) -> String {
    let mut tc = String::new();
    let mut sc = s.chars();
    if let Some(c) = sc.next() {
        tc.extend(c.to_uppercase());
        tc.push_str(sc.as_str());
    }
    tc
}

#[derive(Debug, Copy, Clone, Default)]
struct LetterRec {
    name: char,
    value: i32,
}

impl LetterRec {
    fn from_line(line: &str) -> anyhow::Result<LetterRec> {
        let mut iter = line.split_ascii_whitespace();
        let mut cs = iter.next().context("line must not be empty")?.chars();
        let name = cs.next().context("first word must be a single letter")?;
        if cs.next().is_some() {
            bail!("first word must be a single letter");
        }
        let value = i32::from_str(iter.next().context("missing value")?)
            .context("invalid letter value for letter")?;
        Ok(LetterRec { name, value })
    }
}

#[derive(Debug, Clone)]
struct MgcRec {
    name: String,
    display_name: String,
    tc_name: Ident,
    is_cons: bool,
    is_epf: bool,
    is_vowel: bool,
    is_liquid: bool,
    is_eclipsed: bool,
}

impl MgcRec {
    fn from_line(line: &str) -> anyhow::Result<MgcRec> {
        let mut iter = line.split_ascii_whitespace();
        let name = iter.next().context("line must not be empty")?;
        let mut rec = MgcRec {
            name: name.to_string(),
            display_name: (name.strip_suffix('_').unwrap_or(name)).to_string(),
            tc_name: format_ident!("{}", titlecase(name)),
            is_cons: false,
            is_epf: false,
            is_vowel: false,
            is_liquid: false,
            is_eclipsed: false,
        };
        for tag in iter {
            match tag.strip_prefix('$') {
                Some("cons") => {
                    rec.is_cons = true;
                }
                Some("liquid") => {
                    rec.is_cons = true;
                    rec.is_liquid = true;
                }
                Some("epf") => {
                    rec.is_cons = true;
                    rec.is_epf = true;
                }
                Some("vow") => {
                    rec.is_vowel = true;
                }
                Some("eclipsed") => {
                    rec.is_eclipsed = true;
                }
                Some(_) => bail!("invalid tag: {tag}"),
                None => bail!("tag must start with $"),
            }
        }
        Ok(rec)
    }
}

#[derive(Clone, Debug)]
struct CodaRec {
    contents: String,
    tc_name: Ident,
}

impl CodaRec {
    fn from_line(line: &str) -> anyhow::Result<CodaRec> {
        Ok(CodaRec {
            contents: line.to_owned(),
            tc_name: format_ident!("{}", titlecase(line)),
        })
    }
}

#[derive(Clone, Debug)]
struct Ŋc {
    letters: Vec<LetterRec>,
    mgcs: Vec<MgcRec>,
    codas: Vec<CodaRec>,
    num_simple_codas: usize,
}

#[derive(Clone, Debug)]
struct ŊcLookup<'a> {
    letters_by_name: HashMap<char, &'a LetterRec>,
    mgcs_by_name: HashMap<&'a str, &'a MgcRec>,
}

impl<'a> ŊcLookup<'a> {
    pub fn from_data(data: &'a Ŋc) -> Self {
        let letters_by_name = rev(&data.letters, |letter| letter.name);
        let mgcs_by_name = rev(&data.mgcs, |mgc| mgc.name.as_str());
        Self {
            letters_by_name,
            mgcs_by_name,
        }
    }
}

#[derive(Clone, Debug)]
struct Okvj<'a> {
    data: &'a Ŋc,
    lookup: &'a ŊcLookup<'a>,
}

impl<'a> Okvj<'a> {
    fn imports() -> TokenStream {
        quote! {
            use enum_iterator::Sequence;

            use crate::conv::ConvError;
        }
    }

    fn mgp_def(&self) -> TokenStream {
        let names = self.data.mgcs.iter().map(|m| &m.display_name);
        let cap_names: Vec<_> = self.data.mgcs.iter().map(|m| &m.tc_name).collect();
        let values = self.data.mgcs.iter().map(|mgc| {
            mgc.display_name
                .chars()
                .map(|c| {
                    if c == '·' {
                        // · counts as punctuation
                        return 0;
                    }
                    let letter = *self
                        .lookup
                        .letters_by_name
                        .get(&c)
                        .unwrap_or_else(|| panic!("Character {c} not found in letters.txt"));
                    letter.value
                })
                .sum::<i32>()
        });

        let mut sorted_entries: Vec<_> = self.data.mgcs.iter().collect();
        sorted_entries.sort_unstable_by_key(|e| Reverse(e.name.len()));
        let cond = quote!(&&init && !s.starts_with("·"));
        let parse_clauses = sorted_entries.iter().map(|m| {
            let name = &m.name;
            let cap_name = &m.tc_name;
            let cond = m.is_eclipsed.then_some(&cond);
            quote! {
                if let Some(s) = s.strip_prefix(#name) #cond {
                    return Some((Mgp::#cap_name, s))
                }
            }
        });

        let eclipsed_cap_names: Vec<_> = self
            .data
            .mgcs
            .iter()
            .filter(|m| m.is_eclipsed)
            .map(|m| &m.tc_name)
            .collect();

        quote! {
            /// A *manifested grapheme phrase* in Ŋarâþ Crîþ.
            ///
            /// [`PartialOrd`] and [`Ord`] are intentionally not implemented, as
            /// the eclipsed consonants expand to multiple letters.
            #[doc(alias = "mgc")]
            #[repr(u8)]
            #[derive_const(Clone, PartialEq)]
            #[derive(Copy, Debug, Eq, Hash, Sequence)]
            #[cfg_attr(test, derive(proptest_derive::Arbitrary))]
            #[allow(missing_docs)]
            pub enum Mgp {
                #(#cap_names,)*
            }

            impl Mgp {
                /// Returns a string representation of this manifested grapheme phrase.
                #[inline]
                pub const fn as_str(self) -> &'static str {
                    match self {
                        #(Mgp::#cap_names => #names,)*
                    }
                }

                /// Returns the letter sum of the letters in this manifested grapheme phrase.
                #[inline]
                pub const fn value(self) -> i32 {
                    match self {
                        #(Mgp::#cap_names => #values,)*
                    }
                }

                /// Returns true if this is an eclipsed consonant.
                #[inline]
                pub const fn is_eclipsed(self) -> bool {
                    matches!(self, #(Mgp::#eclipsed_cap_names)|*)
                }

                /// Tries to parse an [`Mgp`] at the beginning of the
                /// string, returning it and the remainder of the
                /// input.
                ///
                /// # Parameters
                ///
                /// * `init` – whether to parse MGPs that can occur only word-initially
                pub fn parse(s: &str, init: bool) -> Option<(Self, &str)> {
                    #(#parse_clauses)*
                    None
                }
            }
        }
    }

    fn cons_def(&self) -> TokenStream {
        let conses: Vec<_> = self.data.mgcs.iter().filter(|m| m.is_cons).collect();
        let names = conses.iter().map(|m| &m.display_name);
        let cap_names: Vec<_> = conses.iter().map(|m| &m.tc_name).collect();
        quote! {
            /// A consonant manifested grapheme phrase in Ŋarâþ Crîþ.
            ///
            /// This excludes ⟦j⟧, which is a semivowel, and the user letters.
            ///
            /// [`PartialOrd`] and [`Ord`] are intentionally not implemented, as
            /// the eclipsed consonants expand to multiple letters.
            #[derive_const(Clone, PartialEq)]
            #[derive(Copy, Debug, Eq, Hash, Sequence)]
            #[allow(missing_docs)]
            pub enum Consonant {
                #(#cap_names,)*
            }

            impl Consonant {
                /// Returns the Unicode codepoints corresponding to this consonant.
                #[inline]
                pub const fn as_str(self) -> &'static str {
                    match self {
                        #(Consonant::#cap_names => #names,)*
                    }
                }
            }
        }
    }

    fn epf_def(&self) -> TokenStream {
        let epfs: Vec<_> = self.data.mgcs.iter().filter(|m| m.is_epf).collect();
        let names = epfs.iter().map(|m| &m.display_name);
        let cap_names: Vec<_> = epfs.iter().map(|m| &m.tc_name).collect();
        quote! {
            /// An *effective plosive or fricative*, or a [consonant][Consonant] whose base letter is a plosive or fricative.
            ///
            /// These consonants can occur in an initial with ⟦r⟧ or ⟦l⟧.
            #[derive_const(Clone, PartialEq)]
            #[derive(Copy, Debug, Eq, Hash, Sequence)]
            #[allow(missing_docs)]
            pub enum Epf {
                #(#cap_names,)*
            }

            impl Epf {
                /// Returns the Unicode codepoints corresponding to this consonant.
                #[inline]
                pub const fn as_str(self) -> &'static str {
                    match self {
                        #(Epf::#cap_names => #names,)*
                    }
                }
            }
        }
    }

    fn liquid_def(&self) -> TokenStream {
        let liquids: Vec<_> = self.data.mgcs.iter().filter(|m| m.is_liquid).collect();
        let names = liquids.iter().map(|m| &m.display_name);
        let cap_names: Vec<_> = liquids.iter().map(|m| &m.tc_name).collect();
        quote! {
            /// Either ⟦r⟧ or ⟦l⟧.
            #[derive_const(Clone, PartialEq)]
            #[derive(Copy, Debug, Eq, Hash, Sequence)]
            #[allow(missing_docs)]
            pub enum Liquid {
                #(#cap_names,)*
            }

            impl Liquid {
                /// Returns the Unicode codepoints corresponding to this consonant.
                #[inline]
                pub const fn as_str(self) -> &'static str {
                    match self {
                        #(Liquid::#cap_names => #names,)*
                    }
                }
            }
        }
    }

    fn vowel_def(&self) -> TokenStream {
        let vowels: Vec<_> = self.data.mgcs.iter().filter(|m| m.is_vowel).collect();
        let names = vowels.iter().map(|m| m.display_name.chars().next());
        let cap_names: Vec<_> = vowels.iter().map(|m| &m.tc_name).collect();
        quote! {
            /// A vowel in Ŋarâþ Crîþ is one of ⟦e o a î i ê ô â u⟧.
            ///
            /// The implementation of [`Ord`] adheres to Cenvos alphabetical order.
            #[repr(u8)]
            #[derive_const(Clone, PartialEq, PartialOrd)]
            #[derive(Copy, Debug, Eq, Ord, Hash, Sequence)]
            #[allow(missing_docs)]
            pub enum Vowel {
                #(#cap_names,)*
            }

            impl Vowel {
                /// Returns the Unicode codepoint corresponding to this vowel.
                ///
                /// Since all vowels are romanized as single codepoints, this method
                /// can return a [`char`].
                #[inline]
                pub const fn as_char(self) -> char {
                    match self {
                        #(Vowel::#cap_names => #names,)*
                    }
                }
            }
        }
    }

    fn coda_def(&self) -> TokenStream {
        let cap_names_terminal: Vec<_> = self.data.codas.iter().map(|m| &m.tc_name).collect();
        let cap_names = &cap_names_terminal[..self.data.num_simple_codas];

        let (mgps_terminal, names_terminal): (Vec<_>, Vec<_>) = self
            .data
            .codas
            .iter()
            .map(|m| {
                let (idents, names) = m
                    .contents
                    .chars()
                    .map(|c| {
                        let mgc = *self
                            .lookup
                            .mgcs_by_name
                            .get(String::from(c).as_str())
                            .unwrap();
                        (&mgc.tc_name, mgc.display_name.as_str())
                    })
                    .unzip::<_, _, Vec<_>, Vec<_>>();
                (idents, names.join(""))
            })
            .unzip();
        let mgps = &mgps_terminal[..self.data.num_simple_codas];
        let names = &names_terminal[..self.data.num_simple_codas];

        quote! {
            /// A coda that can terminate a medial syllable.
            #[repr(u8)]
            #[derive_const(Clone, PartialEq)]
            #[derive(Copy, Debug, Eq, Hash, Sequence)]
            #[allow(missing_docs)]
            pub enum Coda {
                #(#cap_names,)*
            }

            /// A coda that can terminate the last syllable of a word.
            ///
            /// Marginal codas that appear in only a few words, such as ⟦m⟧, are currently
            /// excluded.
            #[repr(u8)]
            #[derive_const(Clone, PartialEq)]
            #[derive(Copy, Debug, Eq, Hash, Sequence)]
            #[allow(missing_docs)]
            pub enum TerminalCoda {
                #(#cap_names_terminal,)*
            }

            impl Coda {
                /// Gets a slice of the [`Mgp`]s in this coda.
                #[inline]
                pub const fn as_mgps(self) -> &'static [Mgp] {
                    match self {
                        #(Coda::#cap_names => &[#(Mgp::#mgps),*],)*
                    }
                }

                /// Gets the string representation of this coda.
                #[inline]
                pub const fn as_str(self) -> &'static str {
                    match self {
                        #(Coda::#cap_names => #names,)*
                    }
                }
            }

            impl TerminalCoda {
                /// Gets a slice of the [`Mgp`]s in this coda.
                #[inline]
                pub const fn as_mgps(self) -> &'static [Mgp] {
                    match self {
                        #(TerminalCoda::#cap_names_terminal => &[#(Mgp::#mgps_terminal),*],)*
                    }
                }

                /// Gets the string representation of this coda.
                #[inline]
                pub const fn as_str(self) -> &'static str {
                    match self {
                        #(TerminalCoda::#cap_names_terminal => #names_terminal,)*
                    }
                }
            }
        }
    }

    fn conv(
        &self,
        supertype: &Ident,
        subtype: &Ident,
        mut subtype_pred: impl FnMut(&MgcRec) -> bool,
    ) -> TokenStream {
        let subtype_elems: Vec<_> = self.data.mgcs.iter().filter(|x| subtype_pred(x)).collect();
        let subtype_elem_idents: Vec<_> = subtype_elems.iter().map(|m| &m.tc_name).collect();
        quote! {
            impl const From<#subtype> for #supertype {
                #[inline]
                fn from(value: #subtype) -> Self {
                    match value {
                        #(#subtype::#subtype_elem_idents => #supertype::#subtype_elem_idents,)*
                    }
                }
            }

            impl const TryFrom<#supertype> for #subtype {
                type Error = ConvError<#supertype, #subtype>;

                #[inline]
                fn try_from(value: #supertype) -> Result<Self, Self::Error> {
                    match value {
                        #(#supertype::#subtype_elem_idents => Ok(#subtype::#subtype_elem_idents),)*
                        _ => Err(ConvError::new(value)),
                    }
                }
            }
        }
    }

    fn all(&self) -> TokenStream {
        let mut stream = TokenStream::new();

        // Definitions
        stream.extend(Self::imports());
        stream.extend(self.mgp_def());
        stream.extend(self.cons_def());
        stream.extend(self.epf_def());
        stream.extend(self.liquid_def());
        stream.extend(self.vowel_def());
        stream.extend(self.coda_def());

        // Conversions
        let mgpid = format_ident!("Mgp");
        let consid = format_ident!("Consonant");
        let epfid = format_ident!("Epf");
        let lqid = format_ident!("Liquid");
        let vowid = format_ident!("Vowel");
        stream.extend(self.conv(&mgpid, &consid, |x| x.is_cons));
        stream.extend(self.conv(&mgpid, &epfid, |x| x.is_epf));
        stream.extend(self.conv(&consid, &epfid, |x| x.is_epf));
        stream.extend(self.conv(&mgpid, &lqid, |x| x.is_liquid));
        stream.extend(self.conv(&consid, &lqid, |x| x.is_liquid));
        stream.extend(self.conv(&mgpid, &vowid, |x| x.is_vowel));

        stream
    }
}

fn main() -> anyhow::Result<()> {
    let out_dir = env::var("OUT_DIR")?;
    println!("cargo:rerun-if-changed=data/");
    println!("cargo:rerun-if-changed=build.rs");
    let letters = {
        let mut letters = Vec::new();
        let letters_file = BufReader::new(File::open("data/letters.txt")?);
        for line in letters_file.lines() {
            let letter = LetterRec::from_line(&line?)?;
            letters.push(letter);
        }
        letters
    };
    let mgcs = {
        let mut mgcs = Vec::new();
        let mgcs_file = BufReader::new(File::open("data/mgc.txt")?);
        for line in mgcs_file.lines() {
            let mgc = MgcRec::from_line(&line?)?;
            mgcs.push(mgc);
        }
        mgcs
    };
    let (codas, num_simple_codas) = {
        let mut num_simple_codas = None;
        let mut codas = vec![CodaRec {
            contents: String::new(),
            tc_name: format_ident!("Empty"),
        }];
        let coda_file = BufReader::new(File::open("data/coda.txt")?);
        for line in coda_file.lines() {
            let line = line?;
            if line == "#CC" {
                num_simple_codas = Some(codas.len());
                continue;
            }
            let coda = CodaRec::from_line(&line)?;
            codas.push(coda);
        }
        let num_simple_codas = num_simple_codas.unwrap_or(codas.len());
        (codas, num_simple_codas)
    };

    let data = Ŋc {
        letters,
        mgcs,
        codas,
        num_simple_codas,
    };
    let lookup = ŊcLookup::from_data(&data);
    let okvj = Okvj {
        data: &data,
        lookup: &lookup,
    };

    let code = okvj.all();
    let code_str = RustFmt::default().format_tokens(code)?;
    {
        let mut out_file = File::create(format!("{out_dir}/data.rs"))?;
        out_file.write_all(code_str.as_bytes())?;
    }

    Ok(())
}
