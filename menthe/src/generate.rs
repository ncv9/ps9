//! Code generation from a menþe program.

use proc_macro2::TokenStream;
use quote::{format_ident, quote};

use crate::{
    item::{Ast, CategoryDecl},
    plugin::Plugin,
};

/// Generates the Rust code for a given menþe program with the given language plugin.
pub fn generate<P: Plugin>(program: &Ast, _plugin: &P) -> TokenStream {
    let mut code = quote! {
        use std::iter::{Iterator, ExactSizeIterator, FusedIterator};
    };

    for category in &program.categories {
        generate_category_decl(category, &mut code);
    }

    code
}

fn generate_category_decl(category: &CategoryDecl, code: &mut TokenStream) {
    let name = category.name.type_name();
    let n = category.values.len();
    let var_names: Vec<_> = category.values.iter().map(|v| v.name.type_name()).collect();
    let var_display_names = category.values.iter().map(|v| &v.display_name.content);
    let is = 0..n;

    let iter_name = format_ident!("{name}Iter");

    code.extend(quote! {
        #[derive(Copy, Clone, PartialEq, Eq, Debug, Hash)]
        pub enum #name {
            #(#var_names),*
        }

        impl #name {
            pub const COUNT: usize = #n;

            pub fn display_name(self) -> &'static str {
                match self {
                    #(#name::#var_names => #var_display_names,)*
                }
            }

            pub fn ordinal(self) -> usize {
                self as usize
            }

            pub fn try_from_ordinal(i: usize) -> Option<Self> {
                match i {
                    #(#is => Some(#name::#var_names),)*
                    _ => None,
                }
            }

            pub fn from_ordinal(i: usize) -> Self {
                Self::try_from_ordinal(i).unwrap()
            }

            pub fn variants() -> #iter_name {
                #iter_name(0)
            }
        }

        #[derive(Clone, Debug)]
        pub struct #iter_name(usize);

        impl Iterator for #iter_name {
            type Item = #name;

            fn next(&mut self) -> Option<Self::Item> {
                let result = #name::try_from_ordinal(self.0);
                if result.is_some() {
                    self.0 += 1;
                }
                result
            }

            fn size_hint(&self) -> (usize, Option<usize>) {
                let k = #name::COUNT - self.0;
                (k, Some(k))
            }
        }

        impl ExactSizeIterator for #iter_name {
            fn len(&self) -> usize {
                #name::COUNT - self.0
            }
        }

        impl FusedIterator for #iter_name {}

        // TODO: generate this is an option is set for nightly features
        // unsafe impl ::std::iter::TrustedLen for #iter_name {}
    });
}
