//! Token types for menþe.

use std::{
    fmt::{Debug, Display},
    ops::{Deref, DerefMut},
};

use heck::{ToSnakeCase, ToUpperCamelCase};
use quote::format_ident;
use smol_str::SmolStr;

use crate::error::{MParseError, MUserError};

/// An identifier in menþe.
///
/// They start with a lowercase letter and contain only lowercase letters, digits, and underscores. Currently, they must be entirely in ASCII.
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Ident(pub SmolStr);

impl Ident {
    /// Gets the type name for this identifier.
    pub fn type_name(&self) -> proc_macro2::Ident {
        format_ident!("{}", self.0.to_upper_camel_case())
    }

    /// Gets the term name for this identifier.
    pub fn term_name(&self) -> proc_macro2::Ident {
        format_ident!("{}", self.0.to_snake_case())
    }
}

impl Display for Ident {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

impl Debug for Ident {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "#{:?}", self.0)
    }
}

/// Parses a plain string literal.
pub fn parse_string_literal(s: &str) -> Result<String, MParseError<'_>> {
    let mut out = String::new();
    let s = s.strip_prefix('\"').unwrap();
    let s = s.strip_suffix('\"').unwrap();
    let mut s = s.chars();
    while let Some(c) = s.next() {
        if c == '\\' {
            let d = match s.next() {
                Some('n') => '\n',
                Some('t') => '\t',
                Some('\\') => '\\',
                Some('\"') => '\"',
                None => Err(MUserError::EscapeSequenceEndedTooEarly)?,
                Some(c) => Err(MUserError::UnrecognizedEscapeSequence(c))?,
            };
            out.push(d);
        } else {
            out.push(c);
        }
    }
    Ok(out)
}

/// Wraps something with location info.
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Located<T> {
    /// The content itself.
    pub content: T,
    /// The byte index of the start of the string that was parsed to give the value.
    pub start: usize,
    /// The byte index of the end of the string that was parsed to give the value.
    pub end: usize,
}

impl<T> Located<T> {
    /// Creates a new [`Located`].
    pub fn new(content: T, start: usize, end: usize) -> Located<T> {
        Located {
            content,
            start,
            end,
        }
    }
}

impl<T> Deref for Located<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.content
    }
}

impl<T> DerefMut for Located<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.content
    }
}

impl<T: Debug> Debug for Located<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self.content, f)?;
        write!(f, "@{}..{}", self.start, self.end)
    }
}
