//! Named sets.

use std::{
    collections::{
        hash_map::{Entry, IntoValues, Values, ValuesMut},
        HashMap,
    },
    fmt::Debug,
    slice, vec,
};

use crate::{
    error::MUserError,
    token::{Ident, Located},
};

/// Something that has a name.
pub trait Named {
    /// Gets the name of the thing.
    fn get_name(&self) -> &Ident;
}

/// Implement [`Named`] for a struct, assuming that it has a `name` field of type [`Ident`].
#[macro_export]
macro_rules! impl_named {
    ($t:ident) => {
        impl $crate::namedset::Named for $t {
            fn get_name(&self) -> &Ident {
                &self.name
            }
        }
    };
}

impl<T> Named for Located<T>
where
    T: Named,
{
    fn get_name(&self) -> &Ident {
        self.content.get_name()
    }
}

/// A set of things indexed by their names.
#[derive(Clone)]
pub struct NamedSet<V: Named> {
    entries: HashMap<Ident, V>,
}

impl<V: Named> NamedSet<V> {
    /// Creates a new [`NamedSet`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Gets the item associated with this name, if any.
    pub fn get<'a>(&'a self, name: &'_ Ident) -> Option<&'a V> {
        self.entries.get(name)
    }

    /// Puts the item in the set, returning an error in the case of a duplicate name.
    pub fn put(&mut self, item: V) -> Result<&mut V, (V, &mut V)> {
        match self.entries.entry(item.get_name().clone()) {
            Entry::Occupied(e) => Err((item, e.into_mut())),
            Entry::Vacant(e) => Ok(e.insert(item)),
        }
    }

    /// Returns an iterator over references to each item.
    pub fn iter(&self) -> Values<Ident, V> {
        self.entries.values()
    }

    /// Returns an iterator over mutable references to each item.
    pub fn iter_mut(&mut self) -> ValuesMut<Ident, V> {
        self.entries.values_mut()
    }

    /// Gets the number of elements in this collection.
    pub fn len(&self) -> usize {
        self.entries.len()
    }

    /// Returns true if this collection is empty.
    pub fn is_empty(&self) -> bool {
        self.entries.is_empty()
    }

    /// Returns the items given as a [`NamedSet`] or raises a duplication error.
    pub fn from_items(
        which: &'static str,
        items: impl IntoIterator<Item = V>,
    ) -> Result<Self, MUserError> {
        let mut result = Self::default();
        for item in items {
            result
                .put(item)
                .map_err(|e| MUserError::duplicate(which, e.0.get_name().clone()))?;
        }
        Ok(result)
    }
}

impl<V: Named> Default for NamedSet<V> {
    fn default() -> Self {
        Self {
            entries: Default::default(),
        }
    }
}

impl<V: Named> IntoIterator for NamedSet<V> {
    type Item = V;

    type IntoIter = IntoValues<Ident, V>;

    fn into_iter(self) -> Self::IntoIter {
        self.entries.into_values()
    }
}

impl<'a, V: Named + 'a> IntoIterator for &'a NamedSet<V> {
    type Item = &'a V;

    type IntoIter = Values<'a, Ident, V>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, V: Named + 'a> IntoIterator for &'a mut NamedSet<V> {
    type Item = &'a mut V;

    type IntoIter = ValuesMut<'a, Ident, V>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<V: Named + Debug> Debug for NamedSet<V> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("NamedSet")?;
        f.debug_list().entries(self.iter()).finish()
    }
}

/// A ordered set of things indexed by their names.
#[derive(Clone)]
pub struct NamedSeq<V: Named> {
    indices: HashMap<Ident, usize>,
    entries: Vec<V>,
}

impl<V: Named> NamedSeq<V> {
    /// Creates a new [`NamedSet`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Gets the item associated with this name, if any.
    pub fn get<'a>(&'a self, name: &'_ Ident) -> Option<&'a V> {
        let i = self.indices.get(name)?;
        Some(&self.entries[*i])
    }

    /// Puts the item in the set, returning an error in the case of a duplicate name.
    pub fn put(&mut self, item: V) -> Result<&mut V, (V, &mut V)> {
        match self.indices.entry(item.get_name().clone()) {
            Entry::Occupied(e) => Err((item, &mut self.entries[*e.get()])),
            Entry::Vacant(e) => {
                e.insert(self.entries.len());
                self.entries.push(item);
                Ok(self.entries.last_mut().unwrap())
            }
        }
    }

    /// Returns an iterator over references to each item.
    pub fn iter(&self) -> slice::Iter<'_, V> {
        self.entries.iter()
    }

    /// Returns an iterator over mutable references to each item.
    pub fn iter_mut(&mut self) -> slice::IterMut<'_, V> {
        self.entries.iter_mut()
    }

    /// Gets the number of elements in this collection.
    pub fn len(&self) -> usize {
        self.indices.len()
    }

    /// Returns true if this collection is empty.
    pub fn is_empty(&self) -> bool {
        self.indices.is_empty()
    }

    /// Returns the items given as a [`NamedSeq`] or raises a duplication error.
    pub fn from_items(
        which: &'static str,
        items: impl IntoIterator<Item = V>,
    ) -> Result<Self, MUserError> {
        let mut result = Self::default();
        for item in items {
            result
                .put(item)
                .map_err(|e| MUserError::duplicate(which, e.0.get_name().clone()))?;
        }
        Ok(result)
    }
}

impl<V: Named> Default for NamedSeq<V> {
    fn default() -> Self {
        Self {
            entries: Default::default(),
            indices: Default::default(),
        }
    }
}

impl<V: Named> IntoIterator for NamedSeq<V> {
    type Item = V;

    type IntoIter = vec::IntoIter<V>;

    fn into_iter(self) -> Self::IntoIter {
        self.entries.into_iter()
    }
}

impl<'a, V: Named + 'a> IntoIterator for &'a NamedSeq<V> {
    type Item = &'a V;

    type IntoIter = slice::Iter<'a, V>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, V: Named + 'a> IntoIterator for &'a mut NamedSeq<V> {
    type Item = &'a mut V;

    type IntoIter = slice::IterMut<'a, V>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<V: Named + Debug> Debug for NamedSeq<V> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("NamedSeq")?;
        f.debug_list().entries(self.iter()).finish()
    }
}
