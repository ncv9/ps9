//! Compound declarations in menþe.

use smol_str::SmolStr;

use crate::{
    error::{MParseError, MUserError},
    impl_named,
    namedset::NamedSeq,
    token::{Ident, Located},
};

/// The AST of a menþe program.
#[derive(Clone, Debug, Default)]
pub struct Ast {
    /// A list of category declarations.
    pub categories: NamedSeq<Located<CategoryDecl>>,
    /// A list of part declarations.
    pub parts: NamedSeq<Located<PartDecl>>,
}

impl Ast {
    /// Creates an [`Ast`] from a list of [items][Item].
    pub fn from_items(items: Vec<Item>) -> Result<Ast, MParseError<'static>> {
        let mut result = Ast::default();
        for item in items {
            match item {
                Item::Category(category) => {
                    result.categories.put(category).map_err(move |(c, _)| {
                        MUserError::duplicate("category", c.content.name.content)
                    })?;
                }
                Item::Part(part) => {
                    result.parts.put(part).map_err(move |(p, _)| {
                        MUserError::duplicate("part", p.content.name.content)
                    })?;
                }
            }
        }
        Ok(result)
    }
}

/// An item that can occur at the top level of a menþe program.
#[derive(Clone, Debug)]
pub enum Item {
    /// A [category declaration][CategoryDecl].
    Category(Located<CategoryDecl>),
    /// A [part declaration][PartDecl].
    Part(Located<PartDecl>),
}

/// A category declaration.
#[derive(Clone, Debug)]
pub struct CategoryDecl {
    /// The name used in code for this category.
    pub name: Located<Ident>,
    /// The display name for this category.
    pub display_name: Located<String>,
    /// The values that this category can take.
    pub values: NamedSeq<Located<CategoryValue>>,
}

impl_named!(CategoryDecl);

/// A value that a category can be set to.
#[derive(Clone, Debug)]
pub struct CategoryValue {
    /// The name used in code for this value.
    pub name: Located<Ident>,
    /// The display name for this value.
    pub display_name: Located<String>,
}

impl_named!(CategoryValue);

/// A part-of-speech declaration.
#[derive(Clone, Debug)]
pub struct PartDecl {
    /// The name used in code for this value.
    pub name: Located<Ident>,
    /// The display name for this value.
    pub display_name: Located<String>,
    /// The abbreviation for this value.
    pub abbreviation: Located<String>,
    /// The part that this part inherits from, if any.
    pub parent: Option<Located<Ident>>,
    /// True if this part was declared with the `Subpart` keyword and
    /// thus is a terminal declaration.
    pub is_terminal: bool,
    /// The list of items in this declaration.
    pub body: PartBody,
}

impl_named!(PartDecl);

/// The body of a [part-of-speech declaration][PartDecl], separated into different types of items.
#[derive(Clone, Debug, Default)]
pub struct PartBody {
    /// The list of [lookup variables][PartLookup].
    pub lookups: NamedSeq<Located<PartLookup>>,
    /// The list of [regular variables][PartVar].
    pub vars: NamedSeq<Located<PartVar>>,
    /// The list of [table declarations][PartTable].
    pub tables: NamedSeq<Located<PartTable>>,
}

impl PartBody {
    /// Creates a [`PartBody`] from a list of [items][PartItem].
    pub fn from_items(items: Vec<PartItem>) -> Result<PartBody, MParseError<'static>> {
        let mut result = PartBody::default();
        for item in items {
            match item {
                PartItem::Lookup(lookup) => {
                    result.lookups.put(lookup).map_err(move |(k, _)| {
                        MUserError::duplicate("lookup", k.content.name.content)
                    })?;
                }
                PartItem::Var(var) => {
                    result.vars.put(var).map_err(move |(v, _)| {
                        MUserError::duplicate("var", v.content.name.content)
                    })?;
                }
                PartItem::Table(table) => {
                    result.tables.put(table).map_err(move |(t, _)| {
                        MUserError::duplicate("table", t.content.name.content)
                    })?;
                }
            }
        }
        Ok(result)
    }
}

/// An item that can occur within a [part-of-speech declaration][PartDecl].
#[derive(Clone, Debug)]
pub enum PartItem {
    /// A [lookup variable][PartLookup].
    Lookup(Located<PartLookup>),
    /// A [regular variable][PartVar].
    Var(Located<PartVar>),
    /// A [table declaration][PartTable].
    Table(Located<PartTable>),
}

/// A `Lookup` declaration in a [part-of-speech declaration][PartDecl].
#[derive(Clone, Debug)]
pub struct PartLookup {
    /// The name used in code for this value.
    pub name: Located<Ident>,
    /// The type of this lookup.
    pub ty: Located<Ident>,
}

impl_named!(PartLookup);

/// A `Var` declaration in a [part-of-speech declaration][PartDecl].
#[derive(Clone, Debug)]
pub struct PartVar {
    /// The name used in code for this value.
    pub name: Located<Ident>,
    /// The type of this var.
    pub ty: Located<Ident>,
}

impl_named!(PartVar);

/// A table declaration in a [part-of-speech declaration][PartDecl].
#[derive(Clone, Debug)]
pub struct PartTable {
    /// The name used in code for this value.
    pub name: Located<Ident>,
    /// The [properties][PartTableProps] of this table, if any.
    pub props: Option<Located<PartTableProps>>,
    /// Any inflections listed in the table.
    pub body: Vec<Located<PartTableInflection>>,
}

impl_named!(PartTable);

/// Properties declared on a table on its first definition.
#[derive(Clone, Debug)]
pub struct PartTableProps {
    /// The display name of this table, if any.
    pub display_name: Located<Option<String>>,
    /// The axes of the table.
    pub axes: Vec<Located<Ident>>,
}

/// An inflection entry in an [inflection table][PartTable].
#[derive(Clone, Debug)]
pub struct PartTableInflection {
    /// The values given to this cell for each axis.
    pub axis_values: Vec<Located<Ident>>,
    /// The expression given to this cell.
    pub value: Located<AstExpr>,
}

/// An expression given as the result of inflection.
#[derive(Clone, Debug)]
pub enum AstExpr {
    /// An identifier, followed by operations.
    Term {
        /// The identifier itself.
        ident: Located<Ident>,
        /// An optional precomputed operation of the form `.<ident>`.
        // TODO: support more general operands for these?
        precomputed_operation: Option<Located<Ident>>,
        /// Zero or more transformations of the form `:<ident>`.
        transformations: Vec<Located<Ident>>,
    },
    /// A [prefixed string literal][PrefixedStringLiteral].
    Literal(Located<PrefixedStringLiteral>),
    /// An *n*-ary append expression.
    Append(Vec<Located<AstExpr>>),
}

/// A string literal with a `#<ident>` prefix.
#[derive(Clone, Debug)]
pub struct PrefixedStringLiteral {
    /// The prefix of the literal.
    pub prefix: SmolStr,
    /// The contents of the string itself.
    pub contents: String,
}

/// Parses a string containing a menþe program into an abstract syntax tree.
pub fn parse_ast(source: &str) -> Result<Ast, MParseError> {
    crate::parser::ProgramParser::new().parse(source)
}
