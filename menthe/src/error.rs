//! Types for error handling.

use lalrpop_util::{lexer::Token, ParseError};
use thiserror::Error;

use crate::token::Ident;

/// A type alias for parsing errors.
pub type MParseError<'input> = ParseError<usize, Token<'input>, MUserError>;

/// Errors returned by menþe itself.
#[derive(Clone, Debug, Error)]
pub enum MUserError {
    /// A duplicate name was declared.
    #[error("duplicate {which} name: {ident}")]
    Duplicate {
        /// The type of item whose name was duplicated.
        which: &'static str,
        /// The identifier of the item that was shared with an earlier one.
        ident: Ident,
    },
    /// An escape sequence in a string literal ended too early.
    #[error("escape sequence ended too early")]
    EscapeSequenceEndedTooEarly,
    /// An unknown escape sequence was used in a string literal.
    #[error("unrecognized escape sequence: \\{0}")]
    UnrecognizedEscapeSequence(char),
}

impl MUserError {
    /// Create an [`MUserError::Duplicate`] error.
    pub fn duplicate(which: &'static str, ident: Ident) -> Self {
        MUserError::Duplicate { which, ident }
    }
}
