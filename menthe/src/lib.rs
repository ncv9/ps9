//! *menþe*, a domain-specific language for reversible inflections.

#![warn(missing_docs)]

pub mod error;
pub mod generate;
pub mod item;
pub mod namedset;
pub mod plugin;
pub mod token;
lalrpop_util::lalrpop_mod!(parser, "/lalrpop/menthe.rs");

#[cfg(test)]
mod tests {
    // use super::*;

    // TODO
}
