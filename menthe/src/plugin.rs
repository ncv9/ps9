//! Language-specific plugins for menþe.

/// A trait defining support for a specific language by menþe.
pub trait Plugin {
    /// The type used to hold the state value.
    type State;
    /// The type used to hold menþe types.
    type Type: AType<Self::State>;
}

/// A trait defining a type over pairs of states.
pub trait AType<State> {
    /// Gets the start state for this type.
    fn start_state(&self) -> State;
    /// Gets the end state for this type.
    fn end_state(&self) -> State;
    /// Returns true if `self` is a subtype of `other`.
    fn is_subtype_of(&self, other: Self) -> bool;
}
