# menþe

*menþe* is a domain-specific language for reversible inflections.

## What it might look like

```text
Category case("case") {
  nom("nominative"),
  acc("accusative"),
  dat("dative"),
  gen("genitive"),
  loc("locative"),
  inst("instrumental"),
  abess("abessive"),
  sembl("semblative"),
}

Category number("number") {
  di("direct"),
  du("dual"),
  pl("plural"),
  st("singulative"),
  gc("generic"),
}

Part noun("noun", "n") {
  // _ ⇒ no display name
  // order of categories matter, and categories can be duplicated
  noun(_, case * number),
}

// Using v9e declensions as a placeholder.
// This inherits from the ‘noun’ part.
Part noun_i("first-declension noun", "nI"): noun {
  // language-specific constructs such as states, types, operations, and concat rules,
  // as well as integration with whatever types are used, could be abstracted
  // away into a language plugin
  // sillier idea: what about providing an assemblage library for arbitrary
  // languages? – would probably turn into a samsara-ish mess, though, to handle
  // ambiguous transitions and alignment requirements. We don’t want to create
  // a box for each transition, after all.
  // ‘Lookup’ means ‘store this attribute of each lexical entry into an index’ –
  // each inflected form is limited to one lookup component.
  Lookup n: stem,
  Lookup l: stem,
  Lookup s: stem,
  // ‘Var’ means that this varies with the lexical entry but should not be indexed.
  Var theta: vowel,
  Var lambda: vowel,
  noun {
    //              v    endpoints between entries must match
    //                     v   or at least have nullable gaps
    (nom, du) -> [n, theta, #nw"c"],
    // NB: n:phi is not the same type as n and is not a lookup component
    (nom, gc) -> [n, theta:plus, n:phi],
    // ...
    (dat, du) -> [n.t, theta, #nw"s"], // n.t will be precomputed on form construction
    // ...
  },
}

// parts are abstract, while subparts are concrete
Subpart noun_i_v("first-declension noun", "nI"): noun_i {
  noun {
    // we only need to define the forms that weren’t defined in noun_i
    (nom, di) -> [n, theta, #nw""],
    (nom, pl) -> [n, theta:pi, #nw""],
    // ...
  },
}
```
