use std::error::Error;

use lalrpop::Configuration;

fn main() -> Result<(), Box<dyn Error>> {
    Configuration::new().process_dir("lalrpop")?;
    Ok(())
}
