use std::{fs::File, io::Read, path::Path};

use menthe::{
    item::{parse_ast, Ast},
    plugin::{AType, Plugin},
};

pub struct SampleLanguagePlugin;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum SampleLanguageState {
    S,
    O,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct SampleLanguageType(SampleLanguageState, SampleLanguageState);

impl Plugin for SampleLanguagePlugin {
    type State = SampleLanguageState;

    type Type = SampleLanguageType;
}

impl AType<SampleLanguageState> for SampleLanguageType {
    fn start_state(&self) -> SampleLanguageState {
        self.0
    }

    fn end_state(&self) -> SampleLanguageState {
        self.1
    }

    fn is_subtype_of(&self, other: Self) -> bool {
        *self == other
    }
}

pub fn source(path: impl AsRef<Path>) -> anyhow::Result<Ast> {
    let mut fh = File::open(path)?;
    let mut source = String::new();
    fh.read_to_string(&mut source)?;

    parse_ast(&source).map_err(|e| anyhow::anyhow!(e.to_string()))
}

pub fn sample_language_source() -> anyhow::Result<Ast> {
    source("tests/common/samplelang.menthe")
}
