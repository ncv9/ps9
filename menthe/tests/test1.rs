use menthe::generate::generate;
use rust_format::Formatter;

use crate::common::SampleLanguagePlugin;

mod common;

#[test]
fn inspect_samplelang_source() {
    let ast = common::sample_language_source().unwrap();
    eprintln!("{ast:#?}");
    let code = generate(&ast, &SampleLanguagePlugin);
    let formatter = rust_format::RustFmt::new();
    eprintln!("{}", formatter.format_tokens(code).unwrap());
}

#[test]
fn inspect_ncv9_source() {
    let _ast = common::source("tests/common/ncv9.menthe").unwrap();
}
